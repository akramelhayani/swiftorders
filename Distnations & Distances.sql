USE [OMDB]
GO
/****** Object:  Table [dbo].[Distances]    Script Date: 05-06-2018 4:46:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Distances](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[StartPoint] [int] NULL,
	[EndPoint] [int] NULL,
	[Distance] [float] NULL,
 CONSTRAINT [PK_Distances] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Distnations]    Script Date: 05-06-2018 4:46:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Distnations](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](150) NULL,
 CONSTRAINT [PK_Distnations] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
