﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Globalization;

namespace SwiftOrders.Reporting
{
    public partial class rpt_ClintIntake : DevExpress.XtraReports.UI.XtraReport
    {
        DataTable ProducT = new DataTable();

        string from, to, clint, qty ="";
        public rpt_ClintIntake(DataTable ProducT ,string from ,string to ,string clint ,string qty )
        {
            InitializeComponent();
            this.ProducT = ProducT;
            this.DataSource = this.ProducT;
            this.from = from;
            this.to = to;
            this.clint = clint;
            this.qty = qty;





        }
        public void LoadData()
        {
            lbl_Clint.Text = clint;
            lbl_DateFrom.Text = from;
            lbl_DateTo.Text = to;
            lbl_Qty.Text  = qty;


            this.DataSource = this.ProducT;
            this.cell_Product.DataBindings.Add("Text", this.DataSource, "ItemId");
            this.Cell_Qty.DataBindings.Add("Text", this.DataSource, "Qty");
            this.Cell_Weight.DataBindings.Add("Text", this.DataSource, "Wieght");

        }

    }
}
