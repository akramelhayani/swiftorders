﻿using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SwiftOrders.Reporting
{
    public class PrintGrid
    {
        static string RePortType = "";

        public static void Print(DevExpress.XtraGrid.GridControl GC, String RePortTypeS)
        {
            PrintingSystem ps = new PrintingSystem();
            CompositeLink composLink = new CompositeLink(ps);
            PrintableComponentLink pcLink1 = new PrintableComponentLink();
            Link linkGrid2Report = new Link();
            Link linkGrid1Report = new Link();
            RePortType = RePortTypeS;

            linkGrid2Report.CreateReportHeaderArea += new CreateAreaEventHandler(CreateHeaderArea);
            linkGrid1Report.CreateReportFooterArea += new CreateAreaEventHandler(CreateFooterArea);
            pcLink1.Component = GC;
            composLink.Links.Add(linkGrid2Report);
            composLink.Links.Add(pcLink1);
            composLink.Links.Add(linkGrid1Report);
            composLink.PaperKind = Properties.Settings.Default.PaperSize;
            composLink.Landscape = Properties.Settings.Default.LandScape;
            composLink.Margins.Left = Properties.Settings.Default.MLeft;
            composLink.Margins.Right = Properties.Settings.Default.MRight;
            composLink.Margins.Bottom = Properties.Settings.Default.MBottom;
            composLink.Margins.Top = Properties.Settings.Default.MTop;
            composLink.RightToLeftLayout = true;
            composLink.ShowPreview ();

         

        }

        static private void CreateFooterArea(object sender, CreateAreaEventArgs e)
        {

            TextBrick tb = new TextBrick();
            tb.Text = "Elhayani Feeds ";
            tb.Font = new Font("Times New Roman", 8, FontStyle.Bold);
            tb.HorzAlignment = DevExpress.Utils.HorzAlignment.Center;
            tb.VertAlignment = DevExpress.Utils.VertAlignment.Center;
            tb.BackColor = Color.White;
            tb.ForeColor = Color.LightGray;
            tb.Rect = new RectangleF(0, 0, e.Graph.ClientPageSize.Width, 15);
            e.Graph.DrawBrick(tb);

        }

        static public void CreateHeaderArea(object sender, CreateAreaEventArgs e)
        {
            TextBrick tb = new TextBrick();
            tb.Text = RePortType;
            tb.Font = new Font("Times New Roman", 12, FontStyle.Bold);
            tb.HorzAlignment = DevExpress.Utils.HorzAlignment.Center;
            tb.VertAlignment = DevExpress.Utils.VertAlignment.Center;
            tb.BackColor = Color.LightGray;
            tb.ForeColor = Color.Black;
            tb.Rect = new RectangleF(0, 0, e.Graph.ClientPageSize.Width, 30);
            e.Graph.DrawBrick(tb);

        }
    }
}
