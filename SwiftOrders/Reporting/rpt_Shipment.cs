﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace SwiftOrders.Reporting
{
    public partial class rpt_Shipment : DevExpress.XtraReports.UI.XtraReport
    {
        DataTable ProducT = new DataTable();
        string PlateNB     = "";
        string date        = "yyyy-MM-dd";
        string totalWieght = "000.00";
        string from        = "";
        public rpt_Shipment(DataTable ProducT, string PlateNB , string date , string totalWieght ,string from  )
        {
            InitializeComponent();
            this.ProducT = ProducT;
            this.DataSource = this.ProducT;
            this.PlateNB      =PlateNB          ;
            this.date         =date             ;
            this.totalWieght  =totalWieght      ;
            this.from = from;


        }
        public void LoadData()
        {
            lbl_Car   .Text= PlateNB;
            lbl_Date  .Text= date;
            lbl_From  .Text= from;
            lbl_Weight.Text= totalWieght;

            this.DataSource = this.ProducT;
            this.cell_Product .DataBindings.Add("Text", this.DataSource, "Product");
            this.Cell_Qty .DataBindings.Add("Text", this.DataSource, "Qty");

        }



    }
}
