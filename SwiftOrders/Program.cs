﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.UserSkins;
using DevExpress.Skins;
using System.Data.SqlClient;

namespace SwiftOrders
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Configration. Con = new SqlConnection(Properties.Settings .Default.OMDBConnectionString );
            Configration.ERPCon = new SqlConnection(Properties.Settings.Default.ERPConnectionString );
            Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            BonusSkins.Register();
            SkinManager.EnableFormSkins();

            Master.MainForm.Show();
            Application.Run( /* new Forms.frm_Locations () */);
        }
    }
}
