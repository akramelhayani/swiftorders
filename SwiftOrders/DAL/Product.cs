﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwiftOrders.DAL
{
    class Product
    {
        public Product()
        {
            IsNew = true;
            ID = Master.GetNewID("ID", "Product");
        }
        public Product(int id)
        {
            DataTable DA = new DataTable();
            DA = Master.SqlDataTable("Select * From Product where ID  = '" + id + "' ");
            IsNew = false;
            ID = (Int32)DA.Rows[0]["ID"];
            ProductID = (Int32)DA.Rows[0]["ProductID"];
            Unit = (string)DA.Rows[0]["Unit"];
            UnitWeight = (Double)DA.Rows[0]["UnitWeight"];
        }
        public Boolean IsNew { get; set; }
        public Int32 ID { get; set; }
        public Int32 ProductID { get; set; }
        public string Unit { get; set; }
        public Double UnitWeight { get; set; }
        public Double Price { get { return ((int)Master.SqlDataTable("Select MediumUOMPrice From ERp.dbo.IC_Item Where itemid = "+ ProductID +"  ").Rows[0][0]); } }


        public void Save()
        {
            if (IsNew == true) { Master.GetNewID("ID", "Product"); Master.SQlCmd("Insert Into Product(ID)Values( " + ID + ") "); }
            string SaveSQltxt = " Update Product set "
             + " ID = '" + ID + "' "
             + ",ProductID = '" + ProductID + "' "
             + ",Unit = '" + Unit + "' "
             + ",UnitWeight = '" + UnitWeight + "' "
             + " from  Product where ID  = '" + ID + "' ";
            Master.SQlCmd(SaveSQltxt);
            IsNew = false;
        }
        public void Delete()
        {
          
            if (Master.ShowDeleteConfrmationDialog(Master.MainForm, LangResorces.ConfrmDeleteStore + "هل تريد حذف هذا الصنف") == true)
            {
                Master.SQlCmd(" delete from Product where ID  = '" + ID + "'");
                IsNew = true;
              
            }
        }
        public static DataTable ToData()
        {
            DataTable DA = new DataTable();
            DA = Master.SqlDataTable("Select * From Product  ");
            return DA;
        }
        public static void ToLookUp(DevExpress.XtraEditors.LookUpEdit lookUp)
        {
            lookUp.Properties.DataSource = ToData();
            lookUp.Properties.ValueMember = "ID";
            lookUp.Properties.DisplayMember = "ProductID";
        }

        public static void ToRepoLookUp(DevExpress.XtraEditors .Repository.RepositoryItemLookUpEdit lookUp)
        {
            lookUp.DataSource = ToData();
            lookUp.ValueMember = "ID";
            lookUp.DisplayMember = "ProductID";
            lookUp.PopulateColumns();
            lookUp.Columns["ID"].Visible = false; 
        }


    }
}
