﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwiftOrders.DAL
{
    class Record
    {
        public Record()
        {
            IsNew = true;
            ID = Master.GetNewID("ID", "Car_Record");
            Date = DateTime.Now;
        }
        public Record(int car ,int type )
        {
           
            Date = DateTime.Now;
            Car = car;
            Type = type;

        }

        public Record(int id)
        {
            if (id == 0) return;
            DataTable DA = new DataTable();
            DA = Master.SqlDataTable("Select * From Car_Record where ID  = '" + id + "' ");
            if (DA.Rows.Count >= 0) return;
            IsNew = false;
            ID = (Int32)DA.Rows[0]["ID"];
            Car = (Int32)DA.Rows[0]["Car"];
            Type = (Int32)DA.Rows[0]["Type"];
            Value = (string)DA.Rows[0]["Value"];
            Date = (DateTime)DA.Rows[0]["Date"];
        }
        public Boolean IsNew { get; set; }
        public Int32 ID { get; set; }
        public Int32 Car { get; set; }
        public Int32 Type { get; set; }
        public string Value { get; set; }
        public DateTime Date { get; set; }
        public void Save( string OldRecoredId = "0")
        {
            Master.SQlCmd(" delete from Car_Record where ID  = '" + OldRecoredId + "'");
            if (IsNew == true) { Master.GetNewID("ID", "Car_Record"); Master.SQlCmd("Insert Into Car_Record(ID)Values( " + ID + ") "); }
            string SaveSQltxt = " Update Car_Record set "
             + " ID = '" + ID + "' "
             + ",Car = '" + Car + "' "
             + ",Type = '" + Type + "' "
             + ",Value = '" + Value + "' "
             + ",Date = '" + Master.SqlDate(Date) + "' "
             + " from  Car_Record where ID  = '" + ID + "' ";
            Master.SQlCmd(SaveSQltxt);
            IsNew = false;
        }
        public void Delete()
        {
              Master.SQlCmd(" delete from Car_Record where ID  = '" + ID + "'");
              
        }
        public static DataTable ToData()
        {
            DataTable DA = new DataTable();
            DA = Master.SqlDataTable("Select * From Car_Record  ");
            return DA;
        }
        public static void ToLookUp(DevExpress.XtraEditors.LookUpEdit lookUp)
        {
            lookUp.Properties.DataSource = ToData();
            lookUp.Properties.ValueMember = " ID ";
            lookUp.Properties.DisplayMember = " Car ";
        }


    }
}
