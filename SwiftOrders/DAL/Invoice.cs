﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwiftOrders.DAL
{
    class Invoice
    {
        public Invoice(DAL.Order order )
        {

            // StoreId
            // Draweraccid
            // Driver 
            // Destination  

            InvoiceCode = "Order"+ order.ID.ToString() ;
            StoreId = 6;
            CustomerId = order.Clint;
            InvoiceDate = DateTime.Now;
            Net = order.TotalPrice;
            Paid = Net;
            Remains = 0;
            DrawerAccountId = 9;
            PurchaseOrderNo = order.ID.ToString();
            VehicleNumber = new Car(order.Car).PlateNB;
            
            CustomerArabicName   = Master.SqlDataTable("Select [CusNameAr]   from [ERP].[dbo].[SL_Customer] Where [CustomerId] = '" + CustomerId + "'").Rows[0][0].ToString() ;
            CusAccountId    =(int) Master.SqlDataTable("Select [AccountId]   from [ERP].[dbo].[SL_Customer] Where [CustomerId] = '" + CustomerId + "'").Rows[0][0] ;
            StoreAccountId = (int) Master.SqlDataTable("Select [SellAccount] from [ERP].[dbo].[IC_Store] Where storeid = '" + StoreId+"' ").Rows[0][0];
            Details = order.Details;
           this.order = order;
        }
        Order order;

        public int ID { get; set; }
        public string InvoiceCode { get; set; }
        public Int32 StoreId { get; set; }
        public Int32 CustomerId { get; set; }
        public DateTime InvoiceDate { get; set; }
        public Double Net { get; set; }
        public Double Paid { get; set; }
        public Double Remains { get; set; }
        public Int32 JornalId { get; set; }
        public Int32 DrawerAccountId { get; set; }
        public string PurchaseOrderNo { get; set; }
        public string DriverName { get; set; }
        public string VehicleNumber { get; set; }
        public string Destination { get; set; }

        public string CustomerArabicName { get; set; }

        public Int32 CusAccountId { get; set; }
        public Int32 StoreAccountId { get; set; }

        public DataTable Details { get; set; }

        public Journal journal;
        public void Save()
        {
            journal = new Journal();
            journal.StoreId = StoreId;
            journal.JNumber = InvoiceCode;
            journal.CustomerArabicName = CustomerArabicName;
            journal.CusAccountId = CusAccountId;
            journal.StoreAccountId = StoreAccountId;
            journal.DrawerAccountId = DrawerAccountId;
            journal.Price = Net;
            


            ID = Convert.ToInt32(Master.SqlDataTable("Use ERP  Select IDENT_CURRENT('SL_Invoice')  ").Rows[0][0])+1;
            journal.SourceId = ID;
            journal.Save();
            JornalId = journal.JCode;
         string SaveSQltxt = "Insert Into [ERP].[dbo].SL_Invoice( "

          + " InvoiceCode       "
          + ",StoreId           "
          + ",CustomerId        "
          + ",InvoiceDate       "
          + ",Net               "
          + ",Paid              "
          + ",Remains           "
          + ",JornalId          "
          + ",DrawerAccountId   "
          + ",PurchaseOrderNo   "
          + ",DriverName        "
          + ",VehicleNumber     "
          + ",Destination       "
          + ",UserID            )"
          + " Values("
          + "   '" + InvoiceCode + "' "
          + " , '" + StoreId + "' "
          + " , '" + CustomerId + "' "
          + " , '" + Master.SqlDate(InvoiceDate) + "' "
          + " , '" + Net + "' "
          + " , '" + Paid + "' "
          + " , '" + Remains + "' "
          + " , '" + JornalId + "' "
          + " , '" + DrawerAccountId + "' "
          + " , '" + PurchaseOrderNo + "' "
          + " , '" + DriverName + "' "
          + " , '" + VehicleNumber + "' "
          + " , '" + Destination + "' "
          + " , '13' )";
            Master.SQlCmd(SaveSQltxt);

           
            for (int i = Details.Rows.Count - 1; i > 0; i--)
            {
                DataTable dt = Master.SqlDataTable("  SELECT  [MediumUOM],[MediumUOMFactor],PurchasePrice FROM [ERP].[dbo].[IC_Item] where [ItemId] = '" + Details.Rows[i]["Product"] + "'");
                int SourceId = Convert.ToInt32(Master.SqlDataTable("Use ERP Select IDENT_CURRENT('SL_InvoiceDetail')").Rows[0][0]) + 1;
                int UOMID = Convert.ToInt32(dt.Rows[0][0]);
                int UOMFactor = Convert.ToInt32(dt.Rows[0][1]);
                double PurchasePrice = Convert.ToDouble(dt.Rows[0][2]);

                Master.SQlCmd("Insert into  [ERP].[dbo].SL_InvoiceDetail ( "

                    + "  SL_InvoiceId    "
                    + " , ItemId         "
                    + " , UOMIndex       "
                    + " , UOMId          "
                    + " , Qty            "
                    + " , SellPrice      "
                    + " , TotalSellPrice "

                    + ") Values ("
                    + ID
                    + "," + Details.Rows[i]["Product"]
                    + "," + "1"
                    + "," + UOMID
                    + "," + Details.Rows[i]["Qty"]
                    + "," + (double)Details.Rows[i]["TotalPrice"] / (double)Details.Rows[i]["Qty"]
                    + "," + (double)Details.Rows[i]["TotalPrice"]
                    + ")  ");

                Master.SQlCmd("Insert into  [ERP].[dbo].IC_ItemStore ( "

                  + "    ProcessId "
                  + " ,  SourceId"
                  + " ,  ItemId"
                  + " ,  StoreId"
                  + " ,  Qty"
                  + " ,  IsInTrns"
                  + " ,  InsertTime"
                  + " ,  SellPrice"
                  + " , PurchasePrice"


                  + ") Values ("
                  + 2
                  + "," + SourceId
                  + "," + Details.Rows[i]["Product"]
                  + "," + StoreId
                  + "," + Convert.ToDouble(Details.Rows[i]["Qty"]) * UOMFactor
                  + ",0"
                  + ",'" + Master.SqlDate(DateTime.Now) + "'"
                  + "," + (double)Details.Rows[i]["TotalPrice"] / (double)Details.Rows[i]["Qty"]
                  + "," + PurchasePrice
                  + ")  ");



            }
            //foreach (DataRow row in Details.Rows )
            //{
            //    DataTable dt = Master.SqlDataTable("  SELECT  [MediumUOM],[MediumUOMFactor],PurchasePrice FROM [ERP].[dbo].[IC_Item] where [ItemId] = '" + row["Product"] +"'");
            //    int SourceId = Convert.ToInt32(Master.SqlDataTable("Use ERP Select IDENT_CURRENT('SL_InvoiceDetail')").Rows[0][0])+1;
            //    int UOMID =Convert.ToInt32( dt.Rows[0][0]);
            //    int UOMFactor = Convert.ToInt32(dt.Rows[0][1]);
            //    double PurchasePrice = Convert.ToDouble(dt.Rows[0][2]);

            //    Master.SQlCmd("Insert into  [ERP].[dbo].SL_InvoiceDetail ( "

            //        + "  SL_InvoiceId    "
            //        + " , ItemId         "
            //        + " , UOMIndex       "
            //        + " , UOMId          "
            //        + " , Qty            "
            //        + " , SellPrice      "
            //        + " , TotalSellPrice "

            //        + ") Values ("
            //        + ID 
            //        +","+ row["Product"]
            //        +","+ "1"
            //        +","+ UOMID 
            //        +","+ row["Qty"]
            //        +","+ (double) row["TotalPrice"] / (double)row["Qty"]
            //        +","+ (double)row["TotalPrice"]
            //        + ")  ");

            //    Master.SQlCmd("Insert into  [ERP].[dbo].IC_ItemStore ( "

            //      + "    ProcessId "
            //      + " ,  SourceId"
            //      + " ,  ItemId"
            //      + " ,  StoreId"
            //      + " ,  Qty"
            //      + " ,  IsInTrns"
            //      + " ,  InsertTime"
            //      + " ,  SellPrice"
            //      + " , PurchasePrice"


            //      + ") Values ("
            //      + 2
            //      + "," + SourceId
            //      + "," + row["Product"]
            //      + "," + StoreId
            //      + "," + Convert.ToDouble( row["Qty"]) * UOMFactor
            //      + ",0"
            //      + ",'" + Master.SqlDate(DateTime.Now)+"'"
            //      + "," + (double)row["TotalPrice"] / (double)row["Qty"]
            //      + "," + PurchasePrice 
            //      + ")  ");



            //}
            order.InvoiceCode = this.InvoiceCode;
            order.Save();

        }


    }

    class Journal
    {
 
        public Int32 JCode { get; set; }
        public string JNotes { get; set; }
    
        public Int32 SourceId { get; set; }
        public DateTime InsertDate { get; set; }
        public Int32 StoreId { get; set; }
        public string JNumber { get; set; }
        public JournalDetail journalDetail_Store { get; set; }
        public JournalDetail journalDetail_Drawer { get; set; }
        public JournalDetail journalDetail_CusDebit { get; set; }
        public JournalDetail journalDetail_CusCredit { get; set; }

        public double Price { get; set; }

        public string CustomerArabicName { get; set; }
        public Int32 CusAccountId { get; set; }
        public Int32 StoreAccountId { get; set; }
        public Int32 DrawerAccountId { get; set; }


        public void Save()
        {
            JCode = Convert.ToInt32(Master.SqlDataTable("Use ERP Select IDENT_CURRENT('ACC_Journal')").Rows[0][0])+1;
            JNotes = "  فاتوره مبيعات رقم " + JNumber + "  الي عميل  " + CustomerArabicName + "  بواسطة مدير الطلبات "; 
            string SaveSQltxt = " Insert Into [ERP].[dbo].ACC_Journal  ( "
             + " JCode       "
             + ",JNotes      "
             + ",ProcessId   "
             + ",SourceId    "
             + ",InsertUser  "
             + ",InsertDate  "
             + ",StoreId     "
             + ",JNumber ) Values (   "
             + "  '" + JCode + "' "
             + ", '" + JNotes + "' "
             + ", '" + 2 + "' "
             + ", '" + SourceId + "' "
             + ", '" + 13 + "' "
             + ", '" + Master.SqlDate(DateTime.Now) + "' "
             + ", '" + StoreId + "' "
             + ", '" + JNumber + "' )";
            Master.SQlCmd(SaveSQltxt);
          
             new JournalDetail(JCode, StoreAccountId  , 0 ,Price, "  فاتوره مبيعات رقم " + JNumber + "  الي عميل  "+ CustomerArabicName + "  بواسطة مدير الطلبات ").Save();
             new JournalDetail(JCode, DrawerAccountId ,Price , 0, " فاتوره مبيعات رقم " + JNumber + "  الي عميل  " + CustomerArabicName + "  بواسطة مدير الطلبات  سداد").Save();
             new JournalDetail(JCode, CusAccountId   , 0 ,Price, "  فاتوره مبيعات رقم " + JNumber + "  الي عميل  "+ CustomerArabicName + "  بواسطة مدير الطلبات ").Save();
             new JournalDetail(JCode, CusAccountId, Price, 0, "  فاتوره مبيعات رقم " + JNumber + "  الي عميل  " + CustomerArabicName + "  بواسطة مدير الطلبات  سداد").Save();

           




        }
    }

    class JournalDetail
    {
        public JournalDetail( int jornalId , int acc , double debit ,double credit , string notes  )
        {
               JournalId  = jornalId ;
                AccountId = acc      ;
               Debit      = debit    ;
               Credit     = credit   ;
               Notes=notes;
        }

        public Int32 JournalId { get; set; }
        public Int32 AccountId { get; set; }
        public double  Debit { get; set; }
        public double Credit { get; set; }
        public string Notes { get; set; }
    
        public void Save()
        {

            string SaveSQltxt = " Insert into  [ERP].[dbo].ACC_JournalDetail ( "
             + " JournalId          "
             + ",AccountId          "
             + ",Debit              "
             + ",Credit             "
             + ",Notes   ) Values ( "
           + "  '" + JournalId + "' "
           + " ,'" + AccountId + "' "
           + " ,'" + Debit + "' "
           + " ,'" + Credit + "' "
           + " ,'" + Notes + "' )";


            Master.SQlCmd(SaveSQltxt);

     
        }

    }
}
