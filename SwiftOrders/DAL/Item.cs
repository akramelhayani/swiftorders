﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwiftOrders.DAL
{
    class Item
    {
     
        public Item(int id)
        {
            ItemId = id;
          
        }
        public Int32 ItemId { get; set; }
        public string ItemNameAr { get { return  Master.SqlDataTable("SELECT [ItemNameAr] FROM [ERP].[dbo].[IC_Item]    where ItemId  = '" + ItemId + "' ").Rows[0][0].ToString(); } }
        public double  MediumUOMFactor { get { return Convert.ToDouble(Master.SqlDataTable("SELECT [MediumUOMFactor] FROM [ERP].[dbo].[IC_Item]    where ItemId  = '" + ItemId + "' ").Rows[0][0]); }  }
        public double  MediumUOMPrice { get { return Convert.ToDouble(  Master.SqlDataTable("SELECT [MediumUOMPrice] FROM [ERP].[dbo].[IC_Item]    where ItemId  = '" + ItemId + "' ").Rows[0][0]); } }

        public static void ToRepoLookUp(DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lookUp)
        {
            lookUp.DataSource = Master.ProducTable;
            lookUp.ValueMember = "ItemId";
            lookUp.DisplayMember = "ItemNameAr";
            lookUp.PopulateColumns();
            lookUp.Columns["ItemId"].Visible = false;
        }
        public static DataTable  ToRepoDataTable()
        {
          return    Master.ProducTable ;
           
        }




    }
}
