﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Windows.Forms;
using DevExpress.XtraBars.Docking2010.Views.WindowsUI;
using DevExpress.XtraBars.Docking2010.Customization;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using System.Threading;
using System.Globalization;
using DevExpress.XtraEditors;
using System.Net;

namespace SwiftOrders
{
    class Master
    {
        public static SwiftOrders.Forms.frm_Main MainForm = new SwiftOrders.Forms.frm_Main();
        public static DataTable ClintsTable  = new DataTable();
        public static DataTable ProducTable = new DataTable();

        public static void GetClintsDate()
        {
            ClintsTable =  SqlDataTable("Select Customerid , CusNameAr ,Tel ,Mobile ,Address,City   from ERP.dbo.Sl_Customer");
        }
        public static void GetProductsDate()
        {
            ProducTable = SqlDataTable(" SELECT [ItemId] ,[ItemNameAr] FROM [ERP].[dbo].[IC_Item] where  itemtype = 2 and IsDeleted = 0  ");
        }
        public static Int32  GetNewID(string IDName, string TableName)
        {
           
               
            SqlDataAdapter DA = new SqlDataAdapter("Select Max("+ IDName + ") From [" + TableName+"]", Configration.Con);
            DataTable DT = new DataTable();
            DA.Fill(DT);
            if(DT.Rows[0][0] == DBNull.Value)
            {
                return 1;
            }
            else
            {
                return Convert.ToInt32( DT.Rows[0][0] )+ 1;
            }
          

        }
        public static bool CheckIfSQLObjectExist(string obj ,string TableName ,string clmName)
        {
            SqlDataAdapter DA = new SqlDataAdapter("Select  COUNT("+ clmName+") From [" + TableName +"] Where ["+ clmName + "] = '"+ obj + "' ", Configration.Con);
            DataTable DT = new DataTable();
            DA.Fill(DT);
            if ((int)DT.Rows[0][0] == 0) return false;
            else return true;
        }

        public static void SQlCmd(string ComandText)
        {
            SqlCommand cmd = new SqlCommand(ComandText, Configration.Con);
           Configration. OpenCon();
            cmd.ExecuteNonQuery();
            Configration. CloseCon();
        }
        public static  DataTable SqlDataTable(string SelectText)
        {
            SqlDataAdapter DA = new SqlDataAdapter(SelectText, Configration. Con);
            DataTable DT = new DataTable();
            DA.Fill(DT);
            return DT;
        }

        public static System.Drawing.Image  GetImageFromByte(Byte[] ByteArray)
        {
            System.Drawing.Image img;
            try
            {
                Byte[] imgbyte = ByteArray ;
                MemoryStream strm = new MemoryStream(imgbyte, false);
                img = System.Drawing.Image.FromStream(strm);
            }
            catch { img = null; }
            return img;
        }

        public static Byte[] GetByteFromImage(System.Drawing.Image image)
        {
            MemoryStream strm = new MemoryStream();
            try
            {

            image.Save(strm, System.Drawing.Imaging.ImageFormat.Jpeg);
            return strm.ToArray();
            }catch
            {
                return strm.ToArray();
            }
        }
        public static string SqlDate(DateTime Date)
        {
            if (Date.ToString("yyyy-MM-dd") == "0001-01-01")
                return Date.ToString("2000-1-1 hh:mm:ss tt");
            else 
                return Date.ToString("yyyy-MM-dd hh:mm:ss tt");
        }

        public static bool ShowDeleteConfrmationDialog(Form frm , string description )
        {
            FlyoutProperties properties = new FlyoutProperties();
            FlyoutAction action = new FlyoutAction() { Caption = "تآكيد ", Description = description };
            FlyoutCommand command1 = new FlyoutCommand() { Text = "حذف", Result = DialogResult.Yes };
            FlyoutCommand command2 = new FlyoutCommand() { Text = "الغاء", Result = DialogResult.No };
            action.Commands.Add(command1);
            action.Commands.Add(command2);
            if (FlyoutDialog.Show(frm, action, properties) == DialogResult.Yes)
            {
                return true;
            }
            else return false;
        }
        public static int PromptForSaveing()
        {
            FlyoutProperties properties = new FlyoutProperties();
            
            FlyoutAction action = new FlyoutAction() { Caption = LangResorces.Confirmation , Description = LangResorces.Msg_SavingPrompt };
            FlyoutCommand command1 = new FlyoutCommand() { Text = LangResorces.Save , Result = DialogResult.Yes };
            FlyoutCommand command2 = new FlyoutCommand() { Text = LangResorces.ExitWithoutSaving, Result = DialogResult.No };
            FlyoutCommand command3 = new FlyoutCommand() { Text = LangResorces.Cancel, Result = DialogResult.Cancel };
            action.Commands.Add(command1);
            action.Commands.Add(command2);
            action.Commands.Add(command3);
            switch (FlyoutDialog.Show(Master.MainForm , action, properties))
            {
                 
                case DialogResult.Cancel: return 2;
                  
                case DialogResult.Yes: return 0;
               
                case DialogResult.No:  return 1;
                   
                default: return 0;
                  
            }
            
        }
        public static void ShowInformMessseg(Form frm ,string Cap, string description)
        {
            FlyoutProperties properties = new FlyoutProperties();
            //properties.Appearance.BackColor 
            FlyoutAction action = new FlyoutAction() { Caption =Cap , Description = description };
            FlyoutCommand command1 = new FlyoutCommand() { Text = "حسناً", Result = DialogResult.Yes };
    
            action.Commands.Add(command1);
        
            FlyoutDialog.Show(frm, action, properties);
            
        }

        public static void SaveGridLayout(GridControl grid,Form frm)
        {
            try
            {
              
                string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + "\\Swift\\XmlLayouts" ;
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
                string xmlFile = path + "\\" + frm.Name +"_"+ grid.Name + ".xml";
               //(grid.MainView as GridView).ActiveFilter.Clear();
                grid.MainView.SaveLayoutToXml(xmlFile);
            }
            catch
            {
            }
        }
        public static void RestoreGridLayout(GridControl grid, Form frm)
        {
            try
            {

                string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + "\\Swift\\XmlLayouts";
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                string xmlFile = path + "\\" + frm.Name + "_" + grid.Name + ".xml";
                if (File.Exists(xmlFile))
                {
                grid.MainView.RestoreLayoutFromXml(xmlFile);
                }
               
            }
            catch
            {
            }
        }
        public static string ReplaceMsgCont(string msg , string replacer)
        {
            string afterreplace = msg.Replace("@", replacer);
            return afterreplace;
        }
        public static void LogUserAction(Form frm , String action ,String Part="", string code = "")
        {
            if (action == "True") action = "0";
            if (action == "False") action = "1";


        }
   
     public static void insertNotfy(int type , int typeid)
        {
            SQlCmd("Insert into [Notfications] ([Type],[TypeID],NotfyTime) Values ('" + type + "','" + typeid + "' , '"+SqlDate(DateTime.Now)+"') ");
        }

    }
}
