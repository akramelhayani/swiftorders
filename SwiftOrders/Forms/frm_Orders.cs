﻿using DevExpress.Data;
using DevExpress.Utils.DragDrop;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SwiftOrders.Forms
{
    public partial class frm_Orders : Form
    {
        int FrmOrderState = 0;
        bool ShowAll = false;
        DataTable OrdersDT = new DataTable();
        public frm_Orders(int orderState = 0, bool showAll = true)
        {
            InitializeComponent();

            FrmOrderState = orderState;
            ShowAll = showAll;
        }


        public virtual void RefreshOrdersDT()
        {
            string stateqrt = "";
            if (!ShowAll)
            {
                stateqrt = "where Status = '" + FrmOrderState + "'";
            }

            string products = "  ";
            foreach (DataRow row in DAL.Item.ToRepoDataTable().Rows) products += "  ,IsNull( (SELECT SUM( [Qty]) FROM [OMDB].[dbo].[OrderProducts] where  [OrderID] = t.ID and [Product] = " + row["ItemId"].ToString() + " ) , 0) as ' " + row["ItemNameAr"].ToString() + "'  ";


            OrdersDT = Master.SqlDataTable("SELECT    "
                + "      [ID] "
                + "     ,[Clint]                                                 "
                + "     ,[Date]                                                  "
                + "     ,[DeliveryDate]                                          "
                + "     ,[TrueDeliveryDate]                                      "
                + "     ,[Car]                                                   "
                + "     ,[Weight]                                                "
                + "     ,[InvoiceCode]                                           "
                + "     ,[OrderGroup]                                            "
                + "     ,[TotalPrice]                                            "
                + "    ,[Status]    ,  OrderSort                                "
                + products
                + " FROM[OMDB].[dbo].[Order] as t   " + stateqrt + "  Order by OrderSort ");
            
            gridControl1.DataSource = OrdersDT;
            for (int i = 12; i < gridView1.Columns.Count; i++)
            {
                GridColumnSummaryItem siTotal = new GridColumnSummaryItem();
                siTotal.SummaryType = SummaryItemType.Sum;
                siTotal.DisplayFormat = "{0}";
                gridView1.Columns[i].Summary.Clear();
                gridView1.Columns[i].Summary.Add(siTotal);
                var total = OrdersDT.Compute("SUM([" + OrdersDT.Columns[i].ColumnName + "])", string.Empty);
                if (total != DBNull.Value && Convert.ToInt32(total) == 0)
                {
                    gridView1.Columns[i].Visible = false;
                }
                else gridView1.Columns[i].Visible = true;
            }
            gridView1.Columns["OrderSort"].Visible = false;

        }
        void SetGridView()
        {
            Master.RestoreGridLayout(gridControl1, this);

            if (!ShowAll && FrmOrderState < 3)
            {
                gridView1.Columns["TrueDeliveryDate"].Visible = false;
                gridView1.Columns["InvoiceCode"].Visible = false;
                gridView1.Columns["TrueDeliveryDate"].OptionsColumn.ShowInCustomizationForm = false;
                gridView1.Columns["InvoiceCode"].OptionsColumn.ShowInCustomizationForm = false;
            }
            if (!ShowAll && FrmOrderState == 0)
            {

                gridView1.OptionsBehavior.Editable = true;
                foreach (GridColumn clm in gridView1.Columns)
                {
                    if (clm.FieldName != "Car" && clm.FieldName != "DeliveryDate") clm.OptionsColumn.AllowEdit = false;
                }

            }
            gridView1.Columns["ID"].Caption = "كود";
            gridView1.Columns["Clint"].Caption = "العميل";
            gridView1.Columns["Date"].Caption = "تاريخ الطلب";
            gridView1.Columns["Date"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            gridView1.Columns["Date"].DisplayFormat.FormatString = "yyyy-MM-dd hh:mm tt";

            gridView1.Columns["DeliveryDate"].Caption = "موعد التسليم";
            gridView1.Columns["TrueDeliveryDate"].Caption = "تاريخ التسليم الفاعلي";
            gridView1.Columns["Car"].Caption = "السياره";
            gridView1.Columns["Weight"].Caption = "الوزن";
            gridView1.Columns["InvoiceCode"].Caption = "كود الفاتوره";
            gridView1.Columns["OrderGroup"].Caption = "المجموعه";
            gridView1.Columns["TotalPrice"].Caption = "قمه الطلبيه";
            gridView1.Columns["Status"].Caption = "الحاله ";


            for (int i = 12; i < gridView1.Columns.Count; i++)
            {
                GridColumnSummaryItem siTotal = new GridColumnSummaryItem();
                siTotal.SummaryType = SummaryItemType.Sum;
                siTotal.DisplayFormat = "{0}";
                gridView1.Columns[i].Summary.Clear();
                gridView1.Columns[i].Summary.Add(siTotal);
                var total = OrdersDT.Compute("SUM([" + OrdersDT.Columns[i].ColumnName + "])", string.Empty);
                if (total != DBNull.Value && Convert.ToInt32(total) == 0)
                {
                    gridView1.Columns[i].Visible = false;
                }
                else gridView1.Columns[i].Visible = true;
            }


            gridView1.Columns["ID"].Fixed = FixedStyle.Left;
            gridView1.Columns["Clint"].Fixed = FixedStyle.Left;
            gridView1.Columns["Date"].Fixed = FixedStyle.Left;
            //gridView1.Columns["Status"].Fixed = FixedStyle.Right;
            gridView1.Columns["Status"].Width = 100;
            gridView1.Columns["Clint"].Width = 150;
            RepositoryItemLookUpEdit RepositoryOrderState = new RepositoryItemLookUpEdit();
            RepositoryOrderState.DataSource = OrderState();
            RepositoryOrderState.ValueMember = "ID";
            RepositoryOrderState.DisplayMember = "Name";
            RepositoryItemLookUpEdit RepositoryClint = new RepositoryItemLookUpEdit();
            RepositoryClint.DataSource = Master.ClintsTable;
            RepositoryClint.ValueMember = "Customerid";
            RepositoryClint.DisplayMember = "CusNameAr";
            RepositoryItemLookUpEdit RepositoryCar = new RepositoryItemLookUpEdit();
            RepositoryCar.DataSource = DAL.Car.ToData(false);
            RepositoryCar.ValueMember = "ID";
            RepositoryCar.DisplayMember = "PlateNB";
            RepositoryItemLookUpEdit RepositoryGroup = new RepositoryItemLookUpEdit();
            RepositoryGroup.DataSource = OrderGroup();
            RepositoryGroup.ValueMember = "ID";
            RepositoryGroup.DisplayMember = "Name";
            gridControl1.RepositoryItems.AddRange(new RepositoryItem[] { RepositoryOrderState, RepositoryClint, RepositoryCar, RepositoryGroup });
            gridView1.Columns["Status"].ColumnEdit = RepositoryOrderState;
            gridView1.Columns["Clint"].ColumnEdit = RepositoryClint;
            gridView1.Columns["Car"].ColumnEdit = RepositoryCar;
            gridView1.Columns["OrderGroup"].ColumnEdit = RepositoryGroup;
            gridView1.Columns["OrderSort"].Visible = false;
            gridView1.ClearSorting();
            gridView1.Columns["OrderSort"].SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;




        }

        private void frm_Orders_Load(object sender, EventArgs e)
        {
            this.Name = "frm_Orders" + FrmOrderState + ShowAll;
            switch (FrmOrderState)
            {
                case 0: this.Text = "جدول الطلبيات بانتظار المراجعه "; break;
                case 1: this.Text = "جدول الطلبيات بانتظار التبليغ "; break;
                case 2: this.Text = "جدول الطلبيات بأنتظار التسليم "; break;
                case 3: this.Text = "جدول الطلبيات التي تم تسليمها "; break;
            }
            if (ShowAll) this.Text = "  جميع الطلبيات  ";

            RefreshOrdersDT();


            if (!ShowAll && FrmOrderState == 0)
            {
                ToolStripMenuItem AcceptToolStripButton = new System.Windows.Forms.ToolStripMenuItem();
                contextMenuStrip1.Items.Add(AcceptToolStripButton);
                AcceptToolStripButton.Size = new System.Drawing.Size(104, 22);
                AcceptToolStripButton.Text = "قبول";
                AcceptToolStripButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
                AcceptToolStripButton.ForeColor = System.Drawing.Color.Green;
                AcceptToolStripButton.Click += AcceptToolStripButton_Click;
            }
            if (!ShowAll && FrmOrderState == 1)
            {
                ToolStripMenuItem ReporStripItem = new System.Windows.Forms.ToolStripMenuItem();
                contextMenuStrip1.Items.Add(ReporStripItem);
                ReporStripItem.Size = new System.Drawing.Size(104, 22);
                ReporStripItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
                ReporStripItem.ForeColor = System.Drawing.Color.RoyalBlue;
                ReporStripItem.Text = "تم التبليغ ";
                ReporStripItem.Click += ReporStripItem_Click;
            }
            if (!ShowAll && FrmOrderState == 2)
            {
                ToolStripMenuItem ConvertToSLInvoices = new System.Windows.Forms.ToolStripMenuItem();
                contextMenuStrip1.Items.Add(ConvertToSLInvoices);
                ConvertToSLInvoices.Size = new System.Drawing.Size(104, 22);
                ConvertToSLInvoices.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
                ConvertToSLInvoices.ForeColor = System.Drawing.Color.Blue;
                ConvertToSLInvoices.Text = "تحويل الي فاتوره مبيعات";
                ConvertToSLInvoices.Click += ConvertToSLInvoices_Click;
            }


            SetGridView();
            HandleBehaviorDragDropEvents();
        }

        private void ConvertToSLInvoices_Click(object sender, EventArgs e)
        {
            if (!(gridView1.FocusedRowHandle >= 0)) return;
           
          

            if (gridView1.GetSelectedRows().Count<int>() >= 1)
            {
      
                List<int> IdLists = new List<int>() ;

                foreach (int handler in gridView1.GetSelectedRows())
                {
                    if (!(Convert.ToInt32(gridView1.GetRowCellValue(handler, "ID")) >= 0)) return;

                    IdLists.Add(Convert.ToInt32(gridView1.GetRowCellValue(handler, "ID")));

                    
                }
                new frm_NewInvoices(IdLists).Show();

            }
        }

        private void ReporStripItem_Click(object sender, EventArgs e)
        {
            if (gridView1.GetSelectedRows().Count<int>() == 1)
            {
                if (!(gridView1.FocusedRowHandle >= 0)) return;
                DAL.Order order = new DAL.Order(Convert.ToInt32(gridView1.GetFocusedRowCellValue("ID")));
                order.Status = 2;
                order.Save();
            }
            else if (gridView1.GetSelectedRows().Count<int>() > 1)
            {
                foreach (int handler in gridView1.GetSelectedRows())
                {
                    if (!(Convert.ToInt32(gridView1.GetRowCellValue(handler, "ID")) >= 0)) return;
                    Master.SQlCmd("Update [Order] Set Status = '2' Where ID = '" + gridView1.GetRowCellValue(handler, "ID") + "' ");
                    DAL.Order.SendRefresh();

                }
            }


        }

        private void AcceptToolStripButton_Click(object sender, EventArgs e)
        {
            if (gridView1.GetSelectedRows().Count<int>() == 1)
            {
                if (!(gridView1.FocusedRowHandle >= 0)) return;
                int rowHandel = gridView1.FocusedRowHandle;
                if (OrdersDT.Rows[rowHandel]["Car"] == DBNull.Value || (int)OrdersDT.Rows[rowHandel]["Car"] <= 0)
                {
                    Master.ShowInformMessseg(this, "خطأ", "تاكد من تعيين سياره للطلبيه ");
                    return;
                }
                DateTime orderdate = Convert.ToDateTime(Convert.ToDateTime(OrdersDT.Rows[rowHandel]["Date"]).ToString("yyyy-MM-dd"));
                DateTime orderDelvarydate = Convert.ToDateTime(Convert.ToDateTime(OrdersDT.Rows[rowHandel]["DeliveryDate"]).ToString("yyyy-MM-dd"));

                if (orderdate.CompareTo(orderDelvarydate) >= 1)
                {
                    Master.ShowInformMessseg(this, "خطأ", "يجب ان يكون تاريخ التسليم بعد تاريخ الطلب");
                    return;
                }



                DAL.Order order = new DAL.Order(Convert.ToInt32(gridView1.GetFocusedRowCellValue("ID")));
                order.Car = Convert.ToInt32(gridView1.GetFocusedRowCellValue("Car"));
                order.DeliveryDate = Convert.ToDateTime(gridView1.GetFocusedRowCellValue("DeliveryDate"));
                order.Status = 1;
                order.Save();
                //RefreshOrdersDT();
                Master.ShowInformMessseg(this, "تم الحفظ بنجاح ", "تم الموافقه علي الطلبيه");

            }
            else if (gridView1.GetSelectedRows().Count<int>() > 1)
            {
                foreach (int handler in gridView1.GetSelectedRows())
                {
                    if (!(Convert.ToInt32(gridView1.GetRowCellValue(handler, "ID")) >= 0)) return;
                    int rowHandel = gridView1.FocusedRowHandle;
                    if (OrdersDT.Rows[rowHandel]["Car"] == DBNull.Value || (int)OrdersDT.Rows[rowHandel]["Car"] <= 0)
                    {
                        //Master.ShowInformMessseg(this, "خطأ", "تاكد من تعيين سياره للطلبيه ");
                        //return;
                    }
                    DateTime orderdate = Convert.ToDateTime(Convert.ToDateTime(OrdersDT.Rows[rowHandel]["Date"]).ToString("yyyy-MM-dd"));
                    DateTime orderDelvarydate = Convert.ToDateTime(Convert.ToDateTime(OrdersDT.Rows[rowHandel]["DeliveryDate"]).ToString("yyyy-MM-dd"));

                    if (orderdate.CompareTo(orderDelvarydate) >= 1)
                    {
                        //Master.ShowInformMessseg(this, "خطأ", "يجب ان يكون تاريخ التسليم بعد تاريخ الطلب");
                        //return;
                    }

                    Master.SQlCmd("Update [Order] Set Car = '" + gridView1.GetFocusedRowCellValue("Car") + "' , "
                        + " DeliveryDate  = '" + Master.SqlDate(Convert.ToDateTime(gridView1.GetFocusedRowCellValue("DeliveryDate"))) + "' ,"
                        + " Status = 1 Where ID = '" + gridView1.GetRowCellValue(handler, "ID") + "' ");

                }
                    DAL.Order.SendRefresh();
            }


        }

        public static DataTable OrderState()
        {
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[] { new DataColumn("ID", typeof(int)), new DataColumn("Name", typeof(string)) });
            for (int i = 0; i < 4; i++) { dt.Rows.Add(); dt.Rows[i][0] = i; }
            dt.Rows[0][1] = "بانتظار المراجعه";
            dt.Rows[1][1] = "بانتظار التبليغ";
            dt.Rows[2][1] = "بأنتظار التسليم";
            dt.Rows[3][1] = "تم التسليم";
            return dt;
        }

        public static DataTable OrderGroup()
        {
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[] { new DataColumn("ID", typeof(int)), new DataColumn("Name", typeof(string)) });
            for (int i = 0; i < 3; i++) { dt.Rows.Add(); dt.Rows[i][0] = i; }
            dt.Rows[0][1] = "المصنع";
            dt.Rows[1][1] = "ميت غمر ";
            dt.Rows[2][1] = "اخري";
            return dt;
        }

        private void اضافهToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Master.MainForm.OpenForm(new frm_NewOrder());

        }

        private void طباعهToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (gridView1.GetSelectedRows().Count<int>() == 1) gridView1.OptionsPrint.PrintSelectedRowsOnly = false;
            else if (gridView1.GetSelectedRows().Count<int>() > 1) gridView1.OptionsPrint.PrintSelectedRowsOnly = true;

            Reporting.PrintGrid.Print(gridControl1, this.Text);
            MessageBox.Show(gridView1.GetSelectedRows().ToString());
        }

        private void حذفToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!(gridView1.FocusedRowHandle >= 0)) return;
            if (!Master.ShowDeleteConfrmationDialog(this, "هل تريد حذف الطلبيات المحدده ؟<")) return;
            string intaray = "0";
            if (gridView1.GetSelectedRows().Count<int>() == 1)
            {
                intaray += "," + gridView1.GetFocusedRowCellValue("ID").ToString();
            }

            else if (gridView1.GetSelectedRows().Count<int>() > 1)
            {
               foreach (int number in gridView1.GetSelectedRows())
                {
                    intaray += "," + gridView1.GetRowCellValue( number,"ID").ToString();
                }
            }
            Master.SQlCmd("Delete From [Order] Where ID in ("+ intaray  +") ");
            Master.SQlCmd(" delete from OrderProducts where OrderID  in (" + intaray + ") ");
            DAL.Order.SendRefresh();

        }

        private void تعديلToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!(gridView1.FocusedRowHandle >= 0)) return;
            Master.MainForm.OpenForm(new frm_EditOrder(Convert.ToInt32(gridView1.GetFocusedRowCellValue("ID"))));

        }

        private void frm_Orders_FormClosing(object sender, FormClosingEventArgs e)
        {
            Master.SaveGridLayout(gridControl1, this);

        }

        private void تحديثToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RefreshOrdersDT();
            Master.MainForm.CalculateNotficationPanal();

        }

        private void frm_Orders_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == Keys.Control)
            {
                switch (e.KeyCode)
                {
                    case Keys.P: طباعهToolStripMenuItem.PerformClick(); break;
                    case Keys.R: RefreshOrdersDT(); ; break;

                }
            }
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            if (!(gridView1.FocusedRowHandle >= 0)) return;
            Master.MainForm.OpenForm(new  frm_EditOrder (Convert.ToInt32(gridView1.GetFocusedRowCellValue("ID"))));
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {

        }

        private void gridView1_MouseMove(object sender, MouseEventArgs e)
        {

        }

        private void gridView1_DragObjectDrop(object sender, DevExpress.XtraGrid.Views.Base.DragObjectDropEventArgs e)
        {
            
        }

        private void gridView1_DragObjectOver(object sender, DevExpress.XtraGrid.Views.Base.DragObjectOverEventArgs e)
        {
          
        }

        public void HandleBehaviorDragDropEvents()
        {
            DragDropBehavior gridControlBehavior = behaviorManager1.GetBehavior<DragDropBehavior>(this.gridView1);
            gridControlBehavior.DragDrop += Behavior_DragDrop;
            gridControlBehavior.DragOver += Behavior_DragOver;
        }
        private void Behavior_DragOver(object sender, DragOverEventArgs e)
        {
            DragOverGridEventArgs args = DragOverGridEventArgs.GetDragOverGridEventArgs(e);
            e.InsertType = args.InsertType;
            e.InsertIndicatorLocation = args.InsertIndicatorLocation;
            e.Action = args.Action;
            Cursor.Current = args.Cursor;
            args.Handled = true;
        }
        private void Behavior_DragDrop(object sender, DevExpress.Utils.DragDrop.DragDropEventArgs e)
        {
            GridView targetGrid = e.Target as GridView;
            GridView sourceGrid = e.Source as GridView;
            if (e.Action == DragDropActions.None || targetGrid != sourceGrid)
                return;
            DataTable sourceTable = sourceGrid.GridControl.DataSource as DataTable;

            Point hitPoint = targetGrid.GridControl.PointToClient(Cursor.Position);
            GridHitInfo hitInfo = targetGrid.CalcHitInfo(hitPoint);

            int[] sourceHandles = e.GetData<int[]>();

            int targetRowHandle = hitInfo.RowHandle;
            int targetRowIndex = targetGrid.GetDataSourceRowIndex(targetRowHandle);
            
            List<DataRow> draggedRows = new List<DataRow>();
            foreach (int sourceHandle in sourceHandles)
            {
                int oldRowIndex = sourceGrid.GetDataSourceRowIndex(sourceHandle);
                DataRow oldRow = sourceTable.Rows[oldRowIndex];
                draggedRows.Add(oldRow);
            }
           
            int newRowIndex;
            //int SortOrderFrom = (int) gridView1.GetRowCellValue( sourceHandles[0] , "OrderSort");
            //int SortOrderTo = (int)gridView1.GetRowCellValue(targetRowHandle, "OrderSort")  ;

            //gridView1.SetRowCellValue(sourceHandles[0], "OrderSort", SortOrderTo + 1);
            //gridView1.SetRowCellValue(SortOrderTo, "OrderSort", sourceHandles[0] - 1 );


            switch (e.InsertType)
            {
                case InsertType.Before:
                    newRowIndex = targetRowIndex > sourceHandles[sourceHandles.Length - 1] ? targetRowIndex - 1 : targetRowIndex;
                    for (int i = draggedRows.Count - 1; i >= 0; i--)
                    {
                        DataRow oldRow = draggedRows[i];
                        DataRow newRow = sourceTable.NewRow();
                        newRow.ItemArray = oldRow.ItemArray;
                        sourceTable.Rows.Remove(oldRow);
                        sourceTable.Rows.InsertAt(newRow, newRowIndex);
                    }
                    break;
                case InsertType.After:
                    newRowIndex = targetRowIndex < sourceHandles[0] ? targetRowIndex + 1 : targetRowIndex;
                    for (int i = 0; i < draggedRows.Count; i++)
                    {
                        DataRow oldRow = draggedRows[i];
                        DataRow newRow = sourceTable.NewRow();
                        newRow.ItemArray = oldRow.ItemArray;
                        sourceTable.Rows.Remove(oldRow);
                        sourceTable.Rows.InsertAt(newRow, newRowIndex);
                    }
                    break;
                default:
                    newRowIndex = -1;
                    break;
            }
            int insertedIndex = targetGrid.GetRowHandle(newRowIndex);
            targetGrid.FocusedRowHandle = insertedIndex;
            targetGrid.SelectRow(targetGrid.FocusedRowHandle);

            int BaseNumber = (int) OrdersDT.Compute("Min(OrderSort)", string.Empty);
            int MaxNumber = (int) OrdersDT.Compute("Max(OrderSort)", string.Empty);
            
            for (int i = 0; i < OrdersDT.Rows .Count ; i++)
            {
                OrdersDT.Rows[i]["OrderSort"] = BaseNumber + i ;
                Master.SQlCmd("Update [Order] Set OrderSort = '"+ BaseNumber + i + "' Where ID = '" + OrdersDT.Rows[i]["ID"] + "' ");
            }
            gridControl1.DataSource = OrdersDT;


        }

    }
}
