﻿using DevExpress.Data;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SwiftOrders.Forms
{
    public partial class frm_EditOrder : Form
    {
        DAL.Order order = new DAL.Order();

        public frm_EditOrder(int orderid )
        {
            InitializeComponent();
            order = new DAL.Order(orderid );

        }
        public void SetData()
        {
   
            order.Clint =(int) LookUpEdit_Clint.EditValue;
            order.Date = DateEdit_Date.DateTime;
            order.DeliveryDate = DateEdit_DeliveryDate.DateTime;
            order.Car =(int) LookUpEdit_Car.EditValue;
            order.OrderGroup =(int) LookUpEdit_OrderGroup.EditValue;
            order.Status =(int) LookUpEdit_Status.EditValue;
            order.Weight = Convert.ToDouble(order.Details.Compute("SUM(Weight)", string.Empty));
            order.TotalPrice = Convert.ToDouble(order.Details.Compute("SUM(TotalPrice)", string.Empty));

        }
        public void GetData()
        {

            TextEdit_ID.EditValue = (Int32)order.ID;
            LookUpEdit_Clint.EditValue = (Int32)order.Clint;
            DateEdit_Date.DateTime = (DateTime)order.Date;
            DateEdit_DeliveryDate.DateTime = (DateTime)order.DeliveryDate;
            LookUpEdit_Car.EditValue = (Int32)order.Car;
            LookUpEdit_OrderGroup.EditValue = (Int32)order.OrderGroup;
            LookUpEdit_Status.EditValue = (Int32)order.Status;
            gridControl1.DataSource = order.Details;

        }
        public void Save()
        {
            SetData();
            order.Save();
        }
        private void btn_Save_Click(object sender, EventArgs e)
        {
            if (!((int)LookUpEdit_Clint.EditValue > 0))
            {
                Master.ShowInformMessseg(this, "خطأ", "برجاء اختيار عميل");
                return;
            }
            if ((order.Details.Rows.Count == 0))
            {
                Master.ShowInformMessseg(this, "خطأ", "يجب ادخال صنف واحد علي الاقل");
                return;
            }
            if (LookUpEdit_Clint.EditValue != null && (int)LookUpEdit_Clint.EditValue > 0)
            {
                Save();
               
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            order.Delete();
        }

        private void frm_EditOrder_Load(object sender, EventArgs e)
        {
            GetData();

            gridView1.PopulateColumns();
            RepositoryItemLookUpEdit repo = new RepositoryItemLookUpEdit();
            RepositoryItemSpinEdit repoSpinEdit = new RepositoryItemSpinEdit();
            DAL.Item.ToRepoLookUp(repo);
            gridControl1.RepositoryItems.Add(repo);
            gridControl1.RepositoryItems.Add(repoSpinEdit);
            gridView1.Columns["Qty"].ColumnEdit = repoSpinEdit;
            gridView1.Columns["Product"].ColumnEdit = repo;
            gridView1.Columns["Weight"].OptionsColumn.AllowEdit = false;
            gridView1.Columns["TotalPrice"].OptionsColumn.AllowEdit = false;
            gridView1.Columns["Product"].Caption = "الصنف";
            gridView1.Columns["Weight"].Caption = "الوزن";
            gridView1.Columns["TotalPrice"].Caption = "السعر";
            gridView1.Columns["Qty"].Caption = "الكميه";

            GridColumnSummaryItem siTotal = new GridColumnSummaryItem();
            siTotal.SummaryType = SummaryItemType.Count;
            siTotal.DisplayFormat = "{0} صنف";
            gridView1.Columns["Product"].Summary.Add(siTotal);

            GridColumnSummaryItem TotalQtySummary = new GridColumnSummaryItem();
            TotalQtySummary.SummaryType = SummaryItemType.Sum;
            TotalQtySummary.DisplayFormat = "{0} عدد";
            gridView1.Columns["Qty"].Summary.Add(TotalQtySummary);

            GridColumnSummaryItem TotalWeightSummary = new GridColumnSummaryItem();
            TotalWeightSummary.SummaryType = SummaryItemType.Sum;
            TotalWeightSummary.DisplayFormat = " {0} كيلو ";
            gridView1.Columns["Weight"].Summary.Add(TotalWeightSummary);

            GridColumnSummaryItem TotalPriceSummary = new GridColumnSummaryItem();
            TotalPriceSummary.SummaryType = SummaryItemType.Sum;
            TotalPriceSummary.DisplayFormat = " {0} جنيه ";
            gridView1.Columns["TotalPrice"].Summary.Add(TotalPriceSummary);
            gridView1.BestFitColumns();
            
            LookUpEdit_Clint.Properties.DataSource = Master.ClintsTable;
            LookUpEdit_Clint.Properties.ValueMember = "Customerid";
            LookUpEdit_Clint.Properties.DisplayMember = "CusNameAr";
    
            DAL.Car.ToLookUp(LookUpEdit_Car);
            LookUpEdit_Status .Properties.DataSource =    frm_Orders.OrderState();
            LookUpEdit_Status.Properties.DisplayMember = "Name";
            LookUpEdit_Status.Properties.ValueMember = "ID";

            LookUpEdit_OrderGroup.Properties.DataSource = frm_Orders.OrderGroup();
            LookUpEdit_OrderGroup.Properties.DisplayMember = "Name";
            LookUpEdit_OrderGroup.Properties.ValueMember = "ID";

        }

        private void gridView1_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "Qty")
            {
                if (e.Value == null) return;
                if (Convert.ToDouble(e.Value) >= 1)
                {
                    if (gridView1.GetFocusedRowCellValue("Product") == DBNull.Value) return;
                    DAL.Item item = new DAL.Item(Convert.ToInt32(gridView1.GetRowCellValue(e.RowHandle, "Product")));
                    gridView1.SetRowCellValue(e.RowHandle, "TotalPrice", (Convert.ToDouble(e.Value)) * item.MediumUOMPrice);
                    gridView1.SetRowCellValue(e.RowHandle, "Weight", ((Convert.ToDouble(e.Value) * item.MediumUOMFactor)));
                }
            }
            if (e.Column.FieldName == "Product" && gridView1.GetFocusedRowCellValue("Qty").GetType() != typeof(double))
            {
                if (Convert.ToDouble(gridView1.GetFocusedRowCellValue("Qty")) >= 1)
                {
                    if (gridView1.GetFocusedRowCellValue("Product") == DBNull.Value) return;
                    DAL.Item item = new DAL.Item(Convert.ToInt32(gridView1.GetRowCellValue(e.RowHandle, "Product")));
                    gridView1.SetRowCellValue(e.RowHandle, "TotalPrice", (Convert.ToDouble(e.Value)) * item.MediumUOMPrice);
                    gridView1.SetRowCellValue(e.RowHandle, "Weight", ((Convert.ToDouble(e.Value) * item.MediumUOMFactor)));
                }
            }
        }

        private void gridView1_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            GridView view = sender as GridView;
            GridColumn clmProduct = view.Columns["Product"];
            GridColumn clmQty = view.Columns["Qty"];

            if (view.GetRowCellValue(e.RowHandle, "Product").GetType() != typeof(int) || !(Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "Product")) > 0))
            {
                e.Valid = false;
                view.SetColumnError(clmProduct, "يجب اختيار الصنف");
            }
            if (view.GetRowCellValue(e.RowHandle, "Qty").GetType() != typeof(double) || !(Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "Qty")) > 0))
            {
                e.Valid = false;
                view.SetColumnError(clmQty, "يجب ان تكون الكميه اكبر من الصفر ");
            }
        }

        private void gridView1_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            GridView view = sender as GridView;
            view.SetRowCellValue(e.RowHandle, "Qty", Convert.ToDouble(0));

        }

        private void gridView1_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;

        }

        private void btn_Save_MouseEnter(object sender, EventArgs e)
        {
            btn_Save.Focus();

        }

        private void frm_EditOrder_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.S && e.Modifiers == Keys.Control)
            {
                btn_Save.PerformClick();
            }
        }

        private void gridControl1_ProcessGridKey(object sender, KeyEventArgs e)
        {

            GridControl grid = sender as GridControl;
            GridView view = grid.FocusedView as GridView;
            if (e.KeyData == Keys.Delete && gridView1 .FocusedRowHandle > 0)
            {
                view.DeleteSelectedRows();
                e.Handled = true;
            }

        }

        private void LookUpEdit_Car_EditValueChanged(object sender, EventArgs e)
        {
            if(LookUpEdit_Car.EditValue != null && (int)LookUpEdit_Car.EditValue > 0)
            {
                DataTable dt = new DataTable();
                dt = Master.SqlDataTable("  SELECT   [Product] , SUM([Qty]) ,SUM([Weight]) FROM  [OMDB].[dbo].[OrderProducts] where [OrderID] in (  Select  [ID]  from  [OMDB].[dbo].[Order] Where [Status] = 2 and Car = '" + LookUpEdit_Car.EditValue  +"' and [DeliveryDate] = '" + DateEdit_DeliveryDate.DateTime.ToString("yyyy-MM-dd ") + "' ) group by Product ");
                gridControl2.DataSource = dt;
                RepositoryItemLookUpEdit repo = new RepositoryItemLookUpEdit();
                DAL.Item.ToRepoLookUp(repo);
                gridControl2.RepositoryItems.Add(repo);
                gridView2.Columns["Product"].ColumnEdit = repo;
                gridView2.Columns["Product"].Caption = "الصنف";
                gridView2.Columns[1].Caption = "الكميه";
                gridView2.Columns[2].Caption = "الوزن";

                GridColumnSummaryItem qtySummary = new GridColumnSummaryItem();
                qtySummary.SummaryType = SummaryItemType.Sum;
                GridColumnSummaryItem weightSummary = new GridColumnSummaryItem();
                weightSummary.SummaryType = SummaryItemType.Sum;
                qtySummary.DisplayFormat = "{0} حوال";
                weightSummary.DisplayFormat = "{0} كيلو";

                gridView2.Columns[1].Summary.Clear();
                gridView2.Columns[1].Summary.Add(qtySummary);
                gridView2.Columns[2].Summary.Clear();
                gridView2.Columns[2].Summary.Add(weightSummary);
            }
        }
    }
}
