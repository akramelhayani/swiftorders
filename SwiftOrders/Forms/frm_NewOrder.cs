﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.Data;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Columns;

namespace SwiftOrders.Forms
{
    public partial class frm_NewOrder : Form
    {
        SwiftOrders.DAL.Order order = new DAL.Order();
        public frm_NewOrder()
        {
            InitializeComponent();
        }

        private void ctrl_NewOrder_Load(object sender, EventArgs e)
        {
            New();
            gridView1.PopulateColumns();
            RepositoryItemLookUpEdit repo = new RepositoryItemLookUpEdit();
            RepositoryItemSpinEdit repoSpinEdit = new RepositoryItemSpinEdit();
            DAL.Item.ToRepoLookUp(repo);
            gridControl1.RepositoryItems.Add(repo);
            gridControl1.RepositoryItems.Add(repoSpinEdit);
            gridView1.Columns["Qty"].ColumnEdit = repoSpinEdit;
            gridView1.Columns["Product"].ColumnEdit = repo;
            gridView1.Columns["Weight"].OptionsColumn.AllowEdit = false;
            gridView1.Columns["TotalPrice"].OptionsColumn.AllowEdit = false;
            gridView1.Columns["Product"].Caption = "الصنف";
            gridView1.Columns["Weight"].Caption = "الوزن";
            gridView1.Columns["TotalPrice"].Caption = "السعر";
            gridView1.Columns["Qty"].Caption = "الكميه";

            GridColumnSummaryItem siTotal = new GridColumnSummaryItem();
            siTotal.SummaryType = SummaryItemType.Count;
            siTotal.DisplayFormat = "{0} صنف";
            gridView1.Columns["Product"].Summary.Add(siTotal);

            GridColumnSummaryItem TotalQtySummary = new GridColumnSummaryItem();
            TotalQtySummary.SummaryType = SummaryItemType.Sum;
            TotalQtySummary.DisplayFormat = "{0} عدد";
            gridView1.Columns["Qty"].Summary.Add(TotalQtySummary);

            GridColumnSummaryItem TotalWeightSummary = new GridColumnSummaryItem();
            TotalWeightSummary.SummaryType = SummaryItemType.Sum ;
            TotalWeightSummary.DisplayFormat = " {0} كيلو ";
            gridView1.Columns["Weight"].Summary.Add(TotalWeightSummary);

            GridColumnSummaryItem TotalPriceSummary = new GridColumnSummaryItem();
            TotalPriceSummary.SummaryType = SummaryItemType.Sum;
            TotalPriceSummary.DisplayFormat = " {0} جنيه ";
            gridView1.Columns["TotalPrice"].Summary.Add(TotalPriceSummary);
            gridView1.BestFitColumns();



            SearchLookUpEdit_Clint.Properties.DataSource = Master.ClintsTable;
            SearchLookUpEdit_Clint.Properties.ValueMember = "Customerid";
            SearchLookUpEdit_Clint.Properties.DisplayMember = "CusNameAr";
            SearchLookUpEdit_Clint.Properties.PopulateViewColumns();
            SearchLookUpEdit_Clint.Properties.View.Columns["Customerid"].Visible = false;
            SearchLookUpEdit_Clint.Properties.View.Columns["CusNameAr"].Caption = "الاسم";
            SearchLookUpEdit_Clint.Properties.View.Columns["Tel"].Caption = "الهاتف";
            SearchLookUpEdit_Clint.Properties.View.Columns["Mobile"].Caption = "المحمول";
            SearchLookUpEdit_Clint.Properties.View.Columns["Address"].Caption = "العنوان";
            SearchLookUpEdit_Clint.Properties.View.Columns["City"].Caption = "المدينة";
        }
        public  void New()
        {
            order = new DAL.Order();
            GetData();
        }
        public void GetData()
        {
            TextEdit_ID.EditValue = (Int32)order.ID;
            SearchLookUpEdit_Clint.EditValue = (Int32)order.Clint;
            DateEdit_Date.DateTime = (DateTime)order.Date;
            LookUpEdit_OrderGroup.SelectedIndex = (Int32)order.OrderGroup;
            gridControl1.DataSource = order.Details;
          

        }
        public void SetData()
        {
            order.Clint =(int) SearchLookUpEdit_Clint.EditValue;
            order.Date = DateEdit_Date.DateTime;
            order.OrderGroup =(int) LookUpEdit_OrderGroup.SelectedIndex;
            order.Weight =Convert.ToDouble( order.Details.Compute("SUM(Weight)", string.Empty));
            order.TotalPrice  = Convert.ToDouble(order.Details.Compute("SUM(TotalPrice)", string.Empty));

        }
        public void Save()
        {
            SetData();
            order.Save();
        }

        private void btn_Save_Click(object sender, EventArgs e)
        {
          
            if (!((int)SearchLookUpEdit_Clint.EditValue > 0))
            {
                Master.ShowInformMessseg(this, "خطأ", "برجاء اختيار عميل");
                return;
            }
            if((order.Details.Rows.Count == 0))
            {
                Master.ShowInformMessseg(this, "خطأ", "يجب ادخال صنف واحد علي الاقل");
                return;
            }
            if( SearchLookUpEdit_Clint.EditValue != null &&(int) SearchLookUpEdit_Clint.EditValue > 0 )
            {
                Save();
                New();
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            New();
        }

        private void gridView1_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
           

        }

        private void gridView1_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
             if (e.Column.FieldName == "Qty"  )
            {
                if (e.Value == null  ) return;
                if (Convert.ToDouble(e.Value) >= 1)
                {
                    if (gridView1.GetFocusedRowCellValue("Product") == DBNull.Value) return;
                    DAL.Item item = new DAL.Item(Convert.ToInt32(gridView1.GetRowCellValue (e.RowHandle,"Product")));
                    gridView1.SetRowCellValue(e.RowHandle, "TotalPrice", (Convert.ToDouble(e.Value)) * item.MediumUOMPrice);
                    gridView1.SetRowCellValue (e.RowHandle ,"Weight", ((Convert.ToDouble(e.Value) * item.MediumUOMFactor)));
                }
            }
            if (e.Column.FieldName == "Product" && gridView1.GetFocusedRowCellValue("Qty").GetType() != typeof(double))
            {
                if (Convert.ToDouble(gridView1.GetFocusedRowCellValue("Qty")) >= 1)
                {
                    if (gridView1.GetFocusedRowCellValue("Product") == DBNull.Value) return;
                    DAL.Item item = new DAL.Item(Convert.ToInt32(gridView1.GetRowCellValue(e.RowHandle, "Product")));
                    gridView1.SetRowCellValue(e.RowHandle, "TotalPrice", (Convert.ToDouble(e.Value)) * item.MediumUOMPrice);
                    gridView1.SetRowCellValue(e.RowHandle, "Weight", ((Convert.ToDouble(e.Value) * item.MediumUOMFactor)));
                }
            }


        }

        private void gridView1_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            GridView view = sender as GridView;
            GridColumn clmProduct = view.Columns["Product"];
            GridColumn clmQty = view.Columns["Qty"];

            if (view.GetRowCellValue(e.RowHandle, "Product").GetType() != typeof(int) || !(Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "Product")) > 0)  )
            {
                e.Valid = false;
                view.SetColumnError(clmProduct, "يجب اختيار الصنف");
            }
            if (view.GetRowCellValue(e.RowHandle, "Qty").GetType() != typeof(double ) || !(Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "Qty")) > 0))
            {
                e.Valid = false;
                view.SetColumnError(clmQty, "يجب ان تكون الكميه اكبر من الصفر ");
            }

        }

        private void gridView1_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            GridView view = sender as GridView;
            view.SetRowCellValue(e.RowHandle, "Qty", Convert.ToDouble(0));
          
        }

        private void gridView1_InvalidValueException(object sender, DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs e)
        {
        }

        private void gridView1_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction ;
        }

        private void btn_Save_MouseEnter(object sender, EventArgs e)
        {
            btn_Save.Focus();

        }

        private void frm_NewOrder_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.S && e.Modifiers == Keys.Control)
            {
                btn_Save.Focus();
                btn_Save.PerformClick();
            }
            if (e.KeyCode == Keys.Escape )
            {
                simpleButton1 .PerformClick();
            }
        }
    }
}
