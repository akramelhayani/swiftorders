﻿using DevExpress.Data;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SwiftOrders.Forms
{
    public partial class frm_CustomerReport : Form
    {
        public frm_CustomerReport()
        {
            InitializeComponent();
        }

        DataTable getReport(int Customer ,DateTime from , DateTime to)
        {
           DataTable dataTable  = 
            Master.SqlDataTable
               (
                   "Use ERP  declare  @CusId int = "+Customer+"                                                                   "
                 + " declare  @FromDate nvarchar(100)  = '" + Master.SqlDate(from) + "'                                               "
                 + " declare  @ToDate nvarchar(100)    = '" + Master.SqlDate(to) + "'                                                 "
                 + " SELECT                                                                                     "
                 + "          t.[ItemId]                                                                        "
                 + "        , sum(t.[Qty]) as Qty                                                               "
                 + "        , sum( [IC_ItemStore].[Qty]) as Wieght                                              "
                 + "  FROM( [SL_InvoiceDetail] as t                                                             "
                 + "        join[SL_Invoice] on t.SL_InvoiceId = [SL_Invoice].[SL_InvoiceId]                    "
                 + "        join[IC_ItemStore] on[IC_ItemStore].[SourceId] = t.SL_InvoiceDetailId )             "
                 + "  where[SL_Invoice].[CustomerId] = @CusId  and[InvoiceDate] between @FromDate and @ToDate   "
                 + "        and[IC_ItemStore].[ProcessId] = 2                                                   "
                 + "         group by t.ItemId                                                                  "
               );



            return dataTable;
        }

        private void frm_CustomerReport_Load(object sender, EventArgs e)
        {
            SearchLookUpEdit_Clint.Properties.DataSource = Master.ClintsTable;
            SearchLookUpEdit_Clint.Properties.ValueMember = "Customerid";
            SearchLookUpEdit_Clint.Properties.DisplayMember = "CusNameAr";
            SearchLookUpEdit_Clint.Properties.PopulateViewColumns();
            SearchLookUpEdit_Clint.Properties.View.Columns["Customerid"].Visible = false;
            SearchLookUpEdit_Clint.Properties.View.Columns["CusNameAr"].Caption = "الاسم";
            SearchLookUpEdit_Clint.Properties.View.Columns["Tel"].Caption = "الهاتف";
            SearchLookUpEdit_Clint.Properties.View.Columns["Mobile"].Caption = "المحمول";
            SearchLookUpEdit_Clint.Properties.View.Columns["Address"].Caption = "العنوان";
            SearchLookUpEdit_Clint.Properties.View.Columns["City"].Caption = "المدينة";

          
        }
        void IntGridView()
        {
            gridView1.PopulateColumns();
            RepositoryItemLookUpEdit repo = new RepositoryItemLookUpEdit();
            DAL.Item.ToRepoLookUp(repo);
            gridControl1.RepositoryItems.Add(repo);
            gridView1.Columns["ItemId"].ColumnEdit = repo;


            GridColumnSummaryItem TotalQtySummary = new GridColumnSummaryItem();
            TotalQtySummary.SummaryType = SummaryItemType.Sum;
            TotalQtySummary.DisplayFormat = "{0} كيلو";
            gridView1.Columns["Wieght"].Summary.Clear();
            gridView1.Columns["Wieght"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom ;
            gridView1.Columns["Wieght"].DisplayFormat.FormatString  ="N0";
            gridView1.Columns["Wieght"].Summary.Add(TotalQtySummary);

            GridColumnSummaryItem TotalWieghtSummary = new GridColumnSummaryItem();
            TotalQtySummary.SummaryType = SummaryItemType.Sum;
            TotalQtySummary.DisplayFormat = "{0} عدد";
            gridView1.Columns["Qty"].Summary.Clear();
            gridView1.Columns["Qty"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            gridView1.Columns["Qty"].DisplayFormat.FormatString = "N0";
            gridView1.Columns["Qty"].Summary.Add(TotalWieghtSummary);

            gridView1.Columns["ItemId"]   .Caption = "الصنف";
            gridView1.Columns["Qty"]      .Caption = "الكميه ";
            gridView1.Columns["Wieght"].Caption = "الوزن";

        }

        private void btn_Save_Click(object sender, EventArgs e)
        {
            if(SearchLookUpEdit_Clint.EditValue is int && (int )SearchLookUpEdit_Clint.EditValue > 0)
            {
                DataTable datatable = getReport((int)SearchLookUpEdit_Clint.EditValue, DateEdit_From.DateTime, DateEDIT_To.DateTime);
                if (datatable.Rows.Count == 0) { DevExpress.XtraEditors.XtraMessageBox.Show("لا يوجد مسحوبات لعرضها ", "", MessageBoxButtons.OK, MessageBoxIcon.Error); return; }

                int Kilo = Convert.ToInt32(datatable.Compute("Sum(Wieght)", string.Empty));
                textEdit2.Text = (Kilo / 1000).ToString() + " طن "  ;
                textEdit1.Text = (Kilo / 1000 * Convert.ToInt32 ( spinEdit1.EditValue) ).ToString() + " جنيه ";
                gridControl1.DataSource  = datatable;
                IntGridView();
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            DataTable ProdctTableForPrint = new DataTable();
            ProdctTableForPrint.Columns.Add("Product");
            ProdctTableForPrint.Columns.Add("Qty");
            ProdctTableForPrint.Columns.Add("Wieght");


            for (int i = 0; i < gridView1.RowCount - 1; i++)
            {
                ProdctTableForPrint.Rows.Add();
                ProdctTableForPrint.Rows[i][0] = gridView1.GetRowCellValue(i, "ItemId");
                ProdctTableForPrint.Rows[i][1] = gridView1.GetRowCellValue(i, "Qty");
                ProdctTableForPrint.Rows[i][2] = gridView1.GetRowCellValue(i, "Wieght");

            }


            Reporting.rpt_ClintIntake  report = new Reporting.rpt_ClintIntake(ProdctTableForPrint,DateEdit_From .Text , DateEDIT_To.Text ,SearchLookUpEdit_Clint.Text ,textEdit2.Text );
            report.LoadData();

            using (ReportPrintTool printtool = new ReportPrintTool(report))
            {
                printtool.ShowRibbonPreviewDialog();
            }
        }
     
    }
}
