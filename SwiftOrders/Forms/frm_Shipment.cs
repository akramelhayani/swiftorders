﻿using DevExpress.Data;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SwiftOrders.Forms
{

    public partial class frm_Shipment : Form
    {
        int FrmOrderState = 0;
        bool ShowAll = false;
        DataTable ShipmentOrdersTable = new DataTable();
        DataTable ShipmentProducTable = new DataTable();
        DataTable ShipmentLocationTable = new DataTable();


        DAL.Shipment shipment = new DAL.Shipment();

        public frm_Shipment()
        {
            InitializeComponent();
            isNew = false;
        }
        public frm_Shipment(int ID)
        {
            InitializeComponent();
            isNew = true;
            shipment = new DAL.Shipment(ID);
        }
        bool isNew = false;
        

        private void frm_Shipment_Load(object sender, EventArgs e)
        {
            DAL.Car.ToLookUp(LookUpEdit_Car);
            DAL.Distnation.ToLookUp(LookUpEdit_Start);
            frm_Drivers.ToLookUp(LookUpEdit_Driver);
            DateEdit_Date.EditValueChanged += LookUpEdit_Car_EditValueChanged;
            LookUpEdit_Start.EditValue = Properties.Settings.Default.DefualtLocationID;
            if (!isNew)
            {
                New();
                SetProductsGridView();
                setLocationGridView();
            }
            else
            {
                this.Text = "مأموريه رقم " + shipment.ID; 
                GetData();
                SetProductsGridView();
                setLocationGridView();
            }


        }
        void SetProductsGridView()
        {

            for (int i = 1; i < gridView4.Columns.Count; i++)
            {
                GridColumnSummaryItem siTotal = new GridColumnSummaryItem();
                siTotal.SummaryType = SummaryItemType.Sum;
                siTotal.DisplayFormat = "{0}";
                gridView4.Columns[i].Summary.Clear();
                gridView4.Columns[i].Summary.Add(siTotal);
            }
            //gridView4.Columns[0].OptionsColumn.AllowEdit = false;
            gridView4.Columns[1].OptionsColumn.AllowEdit = false;
            gridView4.Columns[3].OptionsColumn.AllowEdit = false;

            RepositoryItemLookUpEdit repo = new RepositoryItemLookUpEdit();
            RepositoryItemSpinEdit repoSpinEdit = new RepositoryItemSpinEdit();
            DAL.Item.ToRepoLookUp(repo);
            gridControl4.RepositoryItems.Add(repo);
            gridControl4.RepositoryItems.Add(repoSpinEdit);
            gridView4.Columns["Product"].ColumnEdit = repo;
            gridView4.Columns["ExtraQty"].ColumnEdit = repoSpinEdit;
            gridView4.Columns["ExtraQty"].Caption = "الكميه الاضافيه";
            gridView4.Columns["Product"].Caption = "المنتج";
            gridView4.Columns["Qty"].Caption = "الكميه من الطلبيات";
            gridView4.Columns["TotalQty"].Caption = "اجمالي الكميه";


        }
        void setLocationGridView()
        {
            RepositoryItemLookUpEdit LocationRepo = new RepositoryItemLookUpEdit();
            DAL.Distnation.ToRepoLookUp(LocationRepo);
            gridControl3.RepositoryItems.Add(LocationRepo);
            gridView3.Columns["Location"].ColumnEdit = LocationRepo;
            gridView3.Columns["Distance"].OptionsColumn.AllowEdit = false;

            GridColumnSummaryItem siTotal = new GridColumnSummaryItem();
            siTotal.SummaryType = SummaryItemType.Sum;
            siTotal.DisplayFormat = "{0} كيلو متر";
            gridView3.Columns[1].Summary.Clear();
            gridView3.Columns[1].Summary.Add(siTotal);
        }

        public void SetData()
        {
          
            shipment.Date = DateEdit_Date.DateTime;
            shipment.Start =(int) LookUpEdit_Start.EditValue;
            shipment.Car = (int)LookUpEdit_Car.EditValue;
            shipment.Driver = (int)LookUpEdit_Driver.EditValue;


            //shipment.Measurement_ODO = (int) LookUpEdit_Measurement_ODO.EditValue;
            //shipment.Measurement_Fuel = (int) LookUpEdit_Measurement_Fuel.EditValue;
        }
        public void GetData()
        {
            
            DateEdit_Date.DateTime = (DateTime)shipment.Date;
            LookUpEdit_Start.EditValue = (Int32)shipment.Start;
            LookUpEdit_Car.EditValue = (Int32)shipment.Car;
            LookUpEdit_Driver .EditValue = (Int32)shipment.Driver;
            ShipmentProducTable = Master.SqlDataTable("SELECT [Product],[Qty],[ExtraQty] ,(t.Qty + t.ExtraQty)as TotalQty FROM  [ShipProducts] as t where [ShipID] = '" + shipment.ID+"'");
            ShipmentLocationTable = Master.SqlDataTable("SELECT [Location] ,[Distance] FROM  [ShipLocations] where  [ShipID] = '" + shipment.ID + "' ");
            GetCurrentOrders("SELECT   [OrderID] FROM  [ShipOrders] where [ShipID] =  "+ shipment.ID+"");
            gridControl3.DataSource = ShipmentLocationTable;
            gridControl4.DataSource = ShipmentProducTable;
            SetGridView();

            //LookUpEdit_Measurement_ODO.EditValue = (Int32)shipment.Measurement_ODO;
            //LookUpEdit_Measurement_Fuel.EditValue = (Int32)shipment.Measurement_Fuel;
            SpinEdit_Measurement_ODO.EditValue = new DAL.Record(shipment.Measurement_ODO).Value;
            SpinEdit_Measurement_Fuel.EditValue = new DAL.Record(shipment.Measurement_Fuel).Value;

        }


        public  void New()
        {
          
            shipment = new DAL.Shipment();
            GetData();
           
        }
        public  void Save()
        {
           
            SetData();
            shipment.Save();
         
        }


        public virtual void RefreshOrdersDT(int car )
        {
            string stateqrt = "";
            if (!ShowAll)
            {
                stateqrt = "Where Status = '" + FrmOrderState + "'";
            }

            string products = "  ";
            foreach (DataRow row in DAL.Item.ToRepoDataTable().Rows) products += "  ,IsNull( (SELECT SUM( [Qty]) FROM [OMDB].[dbo].[OrderProducts] where  [OrderID] = t.ID and [Product] = " + row["ItemId"].ToString() + " ) , 0) as ' " + row["ItemNameAr"].ToString() + "'  ";


            ShipmentOrdersTable = Master.SqlDataTable("SELECT    "
                + "      [ID] "
                + "     ,[Clint]                                                 "
                + "     ,[Date]                                                  "
                + "     ,[DeliveryDate]                                          "
                + "                                           "
                + "                                                        "
                + "     ,[Weight]                                                "
                + "                                                "
                + "     ,[OrderGroup]                                            "
                + "     ,[TotalPrice]                                            "
                + "     ,OrderSort                                 "
                + products
                + " FROM[OMDB].[dbo].[Order] as t Where Status = 2 and Car = '" + car + "' and DeliveryDate  = '"+(DateEdit_Date.DateTime).ToString("yyyy-MM-dd") + "'  "
                + "  and ID not in (Select OrderID from ShipOrders) Order by OrderSort ");

            gridControl1.DataSource = ShipmentOrdersTable;
            for (int i = 8; i < gridView1.Columns.Count; i++)
            {
                GridColumnSummaryItem siTotal = new GridColumnSummaryItem();
                siTotal.SummaryType = SummaryItemType.Sum;
                siTotal.DisplayFormat = "{0}";
                gridView1.Columns[i].Summary.Clear();
                gridView1.Columns[i].Summary.Add(siTotal);
                var total = ShipmentOrdersTable.Compute("SUM([" + ShipmentOrdersTable.Columns[i].ColumnName + "])", string.Empty);
                if (total != DBNull.Value && Convert.ToInt32(total) == 0)
                {
                    gridView1.Columns[i].Visible = false;
                }
                else gridView1.Columns[i].Visible = true;
            }
            gridView1.Columns["OrderSort"].Visible = false;
            SetGridView();
        }
        void SetGridView()
        {
            Master.RestoreGridLayout(gridControl1, this);
            
            gridView1.Columns["ID"].Caption = "كود";
            gridView1.Columns["Clint"].Caption = "العميل";
            gridView1.Columns["Date"].Caption = "تاريخ الطلب";
            gridView1.Columns["Date"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            gridView1.Columns["Date"].DisplayFormat.FormatString = "yyyy-MM-dd hh:mm tt";
            gridView1.Columns["DeliveryDate"].Caption = "موعد التسليم";
            gridView1.Columns["Weight"].Caption = "الوزن";
            gridView1.Columns["OrderGroup"].Caption = "المجموعه";
            gridView1.Columns["TotalPrice"].Caption = "قمه الطلبيه";
      

            for (int i = 8; i < gridView1.Columns.Count; i++)
            {
                GridColumnSummaryItem siTotal = new GridColumnSummaryItem();
                siTotal.SummaryType = SummaryItemType.Sum;
                siTotal.DisplayFormat = "{0}";
                gridView1.Columns[i].Summary.Clear();
                gridView1.Columns[i].Summary.Add(siTotal);
                var total = ShipmentOrdersTable.Compute("SUM([" + ShipmentOrdersTable.Columns[i].ColumnName + "])", string.Empty);
                if (total != DBNull.Value && Convert.ToInt32(total) == 0)
                {
                    gridView1.Columns[i].Visible = false;
                }
                else gridView1.Columns[i].Visible = true;
            }
            for (int i = 1; i < gridView1.Columns.Count; i++)
            {
                gridView1.Columns[i].OptionsColumn.AllowEdit = false;

            }

            gridView1.Columns["ID"].Fixed = FixedStyle.Left;
            gridView1.Columns["Clint"].Fixed = FixedStyle.Left;
            gridView1.Columns["Date"].Fixed = FixedStyle.Left;
            //gridView1.Columns["Status"].Fixed = FixedStyle.Right;
          
            gridView1.Columns["Clint"].Width = 150;
            RepositoryItemLookUpEdit RepositoryOrderState = new RepositoryItemLookUpEdit();
            RepositoryOrderState.DataSource = frm_Orders.OrderState();
            RepositoryOrderState.ValueMember = "ID";
            RepositoryOrderState.DisplayMember = "Name";
            RepositoryItemLookUpEdit RepositoryClint = new RepositoryItemLookUpEdit();
            RepositoryClint.DataSource = Master.ClintsTable;
            RepositoryClint.ValueMember = "Customerid";
            RepositoryClint.DisplayMember = "CusNameAr";
            RepositoryItemLookUpEdit RepositoryCar = new RepositoryItemLookUpEdit();
            RepositoryCar.DataSource = DAL.Car.ToData(false);
            RepositoryCar.ValueMember = "ID";
            RepositoryCar.DisplayMember = "PlateNB";
            RepositoryItemLookUpEdit RepositoryGroup = new RepositoryItemLookUpEdit();
            RepositoryGroup.DataSource =frm_Orders.OrderGroup();
            RepositoryGroup.ValueMember = "ID";
            RepositoryGroup.DisplayMember = "Name";

            RepositoryItemLookUpEdit RepositoryOrdersEdit = new RepositoryItemLookUpEdit();
            RepositoryOrdersEdit.DataSource = Master.SqlDataTable("Select ID , CusNameAr as 'Clint' ,Weight ,Date  FROM ( [Order] as t join ERP.dbo.SL_Customer  on Customerid = Clint )  ");
            RepositoryOrdersEdit.ValueMember = "ID";
            RepositoryOrdersEdit.DisplayMember = "ID";
            gridControl1.RepositoryItems.Add(RepositoryOrdersEdit);
            gridView1.Columns["ID"].ColumnEdit = RepositoryOrdersEdit;



            gridControl1.RepositoryItems.AddRange(new RepositoryItem[] { RepositoryOrderState, RepositoryClint, RepositoryCar, RepositoryGroup });
         
            gridView1.Columns["Clint"].ColumnEdit = RepositoryClint;
      
            gridView1.Columns["OrderGroup"].ColumnEdit = RepositoryGroup;
            gridView1.Columns["OrderSort"].Visible = false;
            gridView1.ClearSorting();
            gridView1.Columns["OrderSort"].SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;




        }

        private void LookUpEdit_Car_EditValueChanged(object sender, EventArgs e)
        {
           
            if(LookUpEdit_Car.EditValue != null && Convert.ToInt32(LookUpEdit_Car.EditValue) > 0)
            {
                LookUpEdit_Driver.EditValue = new DAL.Car(Convert.ToInt32(LookUpEdit_Car.EditValue)).Driver;
                if (ShipmentOrdersTable.Rows.Count == 0)
                    RefreshOrdersDT(Convert.ToInt32(LookUpEdit_Car.EditValue));
                if (LookUpEdit_Car.Text == "خارجية")
                {
                    SpinEdit_Measurement_Fuel.Enabled = SpinEdit_Measurement_ODO.Enabled = false;
                }
                else
                {
                    SpinEdit_Measurement_Fuel.Enabled = SpinEdit_Measurement_ODO.Enabled = true ;

                }
            }

        }

        private void gridView1_CustomRowCellEditForEditing(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            if(e.Column.FieldName == "ID")
            {
                string IDs = "";
                for (int i = 0; i < gridView1.RowCount ; i++)
                {
                    IDs += "," + "'"+ gridView1.GetRowCellValue(i, "ID")+"'";
                }

                RepositoryItemLookUpEdit CustomRepositoryEdit = new RepositoryItemLookUpEdit();
                CustomRepositoryEdit.DataSource = Master.SqlDataTable("Select ID , CusNameAr as 'Clint' ,Weight ,Date  FROM ( [Order] as t join ERP.dbo.SL_Customer  on Customerid = Clint )"
                +" where  Status = 2                                "
                +" and ID not in (Select OrderID from ShipOrders)   "
                +" and ID not in (0"+IDs+") Order by OrderSort ");
                CustomRepositoryEdit.ValueMember = "ID";
                CustomRepositoryEdit.DisplayMember = "ID";
                gridControl1.RepositoryItems.Add(CustomRepositoryEdit);
                e.RepositoryItem = CustomRepositoryEdit;
            }
        }

        private void gridView1_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "ID" && e.Value .GetType() == typeof(int))
            {
                
                string IDs = "0,'"+ e.Value + "'";
               
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    IDs += "," + "'" + gridView1.GetRowCellValue(i, "ID") + "'";
                }

                GetCurrentOrders(IDs);
                


            }
        }
        void GetCurrentOrders(string ids = "")
        {
            string products = "  ";
            foreach (DataRow row in DAL.Item.ToRepoDataTable().Rows) products += "  ,IsNull( (SELECT SUM( [Qty]) FROM [OMDB].[dbo].[OrderProducts] where  [OrderID] = t.ID and [Product] = " + row["ItemId"].ToString() + " ) , 0) as ' " + row["ItemNameAr"].ToString() + "'  ";

            string IDs = ids;
         
            ShipmentOrdersTable = Master.SqlDataTable("SELECT    "
                + "      [ID] "
                + "     ,[Clint]                                                 "
                + "     ,[Date]                                                  "
                + "     ,[DeliveryDate]                                          "
                + "                                           "
                + "                                                        "
                + "     ,[Weight]                                                "
                + "                                                "
                + "     ,[OrderGroup]                                            "
                + "     ,[TotalPrice]                                            "
                + "     ,OrderSort                                 "
                + products
                + " FROM[OMDB].[dbo].[Order] as t Where    "
                + "    ID in (" + IDs + ") Order by OrderSort ");

            gridControl1.DataSource = ShipmentOrdersTable;

            for (int i = 8; i < gridView1.Columns.Count; i++)
            {
                GridColumnSummaryItem siTotal = new GridColumnSummaryItem();
                siTotal.SummaryType = SummaryItemType.Sum;
                siTotal.DisplayFormat = "{0}";
                gridView1.Columns[i].Summary.Clear();
                gridView1.Columns[i].Summary.Add(siTotal);
                var total = ShipmentOrdersTable.Compute("SUM([" + ShipmentOrdersTable.Columns[i].ColumnName + "])", string.Empty);
                if (total != DBNull.Value && Convert.ToInt32(total) == 0)
                {
                    gridView1.Columns[i].Visible = false;
                }
                else gridView1.Columns[i].Visible = true;
            }
            gridView1.Columns["OrderSort"].Visible = false;
        }
        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle >= 0 && gridView1.GetFocusedRowCellValue("ID").GetType() == typeof(int))
            {
                new Forms.frm_EditOrder((int)gridView1.GetFocusedRowCellValue("ID")).ShowDialog();
                string IDs = "0";
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    IDs += "," + "'" + gridView1.GetRowCellValue(i, "ID") + "'";
                }

                GetCurrentOrders(IDs);
            }



        }

        private void gridView1_DataSourceChanged(object sender, EventArgs e)
        {
            ShipmentProducTable.AcceptChanges();
            foreach (DataRow  row in ShipmentProducTable .Rows)
            {
                row["Qty"] = 0;
            }
            for (int i = 8; i < ShipmentOrdersTable.Columns.Count; i++)
            {
                DataColumn  clm = ShipmentOrdersTable.Columns[i];
                var total = ShipmentOrdersTable.Compute("SUM([" + clm .ColumnName + "])", string.Empty);
                if (total != DBNull.Value && Convert.ToInt32(total) > 0)
                {
                    // Get column product id 
                    int itemid = Convert.ToInt32(Master.SqlDataTable(" SELECT [ItemId]  FROM [ERP].[dbo].[IC_Item] where   ItemNameAr = N'" + clm.ColumnName.TrimStart() +"'  ").Rows[0][0]);
                    List<DataRow> row = ShipmentProducTable.Select("Product = "+itemid+"").ToList<DataRow>();
                    if (row.Count == 0)
                    {
                        ShipmentProducTable.Rows.Add();
                        int last = ShipmentProducTable.Rows.Count -1 ;
                        ShipmentProducTable.Rows[last]["Product"] = itemid;
                        ShipmentProducTable.Rows[last]["Qty"] = total;
                    } else if  (row.Count == 1 )
                    {
                        row[0]["Qty"] = total ;
                    }
                    gridControl4.DataSource = ShipmentProducTable;
                }
            }
            
            CalculatTotalProductsColumn();

        }

        private void gridView4_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            //GridView view = sender as GridView;
            //GridColumn clmProduct = view.Columns["Product"];
            //GridColumn clmQty = view.Columns["ExtraQty"];

            //if ( view.GetRowCellValue(e.RowHandle, "Product") == null  )
            //{
            //    e.Valid = false;
            //    view.SetColumnError(clmProduct, "يجب اختيار الصنف");
            //}
            //if (view.GetRowCellValue(e.RowHandle, "ExtraQty") == null || !( Convert.ToDouble(view.GetRowCellValue(e.RowHandle, "ExtraQty")) > 0))
            //{
            //    if (view.GetRowCellValue(e.RowHandle, "Qty")  is int||  Convert.ToDouble(view.GetRowCellValue(e.RowHandle, "Qty")) == 0)
            //    {
            //        e.Valid = false;
            //        view.SetColumnError(clmQty, "يجب ان تكون الكميه اكبر من الصفر ");
            //    }
              
            //}
        }
        Boolean SusspendCalculation = false;
        private void gridView4_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            SusspendCalculation = true;
            GridView view = sender as GridView;
            view.SetRowCellValue(e.RowHandle, "ExtraQty", Convert.ToDouble(0));
            view.SetRowCellValue(e.RowHandle, "Qty", Convert.ToDouble(0));
            SusspendCalculation = false;

        }

        private void gridView4_CustomRowCellEditForEditing(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.Column.FieldName == "Product")
            {

                 string IDs = "0";
                 for (int i = 0; i < gridView4.RowCount; i++)
                 {
                     IDs += "," + "'" + gridView4.GetRowCellValue(i, "Product") + "'";
                 }
                 RepositoryItemLookUpEdit repo = new RepositoryItemLookUpEdit();
                 repo.DataSource = Master.SqlDataTable(" SELECT [ItemId] ,[ItemNameAr] FROM [ERP].[dbo].[IC_Item] where  itemtype = 2 and IsDeleted = 0 and ItemId not in ("+IDs+")  ");
                 repo.ValueMember = "ItemId";
                 repo.DisplayMember = "ItemNameAr";
                 repo.PopulateColumns();
                 repo.Columns["ItemId"].Visible = false;
                 e.RepositoryItem = repo;

            }
           
        }

        private void gridView4_ColumnChanged(object sender, EventArgs e)
        {
            
        }

        private void gridView4_FocusedRowObjectChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowObjectChangedEventArgs e)
        {
           if(gridView4.GetFocusedRowCellValue("Qty") != null && gridView4.GetFocusedRowCellValue("Qty") != DBNull.Value && (double)gridView4.GetFocusedRowCellValue("Qty") > 0)
            {
                gridView4.Columns["Product"].OptionsColumn.AllowEdit = false;

            }
            else
            {
                gridView4.Columns["Product"].OptionsColumn.AllowEdit = true ;

            }
        }

        private void gridView4_Click(object sender, EventArgs e)
        {
           
        }
      
        void SetProgressBarVal(int val)
        {
            if (LookUpEdit_Car.EditValue != null && (int)LookUpEdit_Car.EditValue > 0 )
            {

                progressBar1.Maximum = (int)new DAL.Car((int)LookUpEdit_Car.EditValue).CargoCap;
                if (val > progressBar1.Maximum)
                {
                    progressBar1.Value = progressBar1.Maximum;
                    ModifyProgressBarColor. SetState(progressBar1,2);
                }
                else
                {
                    progressBar1.Value = val;
                    ModifyProgressBarColor .SetState(progressBar1, 1);

                }
            }
            label1.Text = val.ToString() + " من " + progressBar1.Maximum.ToString() + " طن ";

        }

        void CalculatTotalProductsColumn()
        {
            if (SusspendCalculation) return;
            double TotalWieght = 0;
            ShipmentProducTable.AcceptChanges();
            foreach (DataRow  row  in ShipmentProducTable.Rows)
            {

                if (row["Qty"] == null || row["Qty"] == DBNull.Value) row["Qty"] = 0;
                if (row["ExtraQty"] == null || row["ExtraQty"] == DBNull.Value) row["ExtraQty"] = 0;
                row["TotalQty"] = (Double)row["ExtraQty"] + (Double)row["Qty"];
                TotalWieght += (double)row["TotalQty"] * new DAL.Item((int)row["Product"]).MediumUOMFactor;
            }
            ShipmentProducTable.AcceptChanges();
            foreach (DataRow row in ShipmentProducTable.Rows)
            {
                if (row["Qty"].GetType() == typeof(double) && row["ExtraQty"] .GetType() == typeof(double) && (Double)row["ExtraQty"] + (Double)row["Qty"] == 0) 
                row.Delete();
            }
            ShipmentProducTable.AcceptChanges();
            SetProgressBarVal((int)TotalWieght);
            gridControl4.DataSource = ShipmentProducTable;
        }
        private void gridView4_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "Qty" || e.Column.FieldName == "ExtraQty")
            {
                CalculatTotalProductsColumn();
            }
        }

        private void gridView3_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "Location")
                if (LookUpEdit_Start.EditValue?.GetType() == typeof(int) && (int)LookUpEdit_Start.EditValue > 0)
                {
                    if (gridView3.RowCount == 1)
                        gridView3.SetRowCellValue(e.RowHandle, "Distance", DAL.Distnation.GetLocalDistance((int)LookUpEdit_Start.EditValue, (int)e.Value));
                    else if (gridView3.RowCount > 1)
                        gridView3.SetRowCellValue(e.RowHandle, "Distance", DAL.Distnation.GetLocalDistance((int)gridView3.GetRowCellValue((e.RowHandle >= 0) ? e.RowHandle - 1 : gridView3.RowCount - 2, "Location"), (int)e.Value));
                }
          
        }

        private void gridView3_CustomRowCellEditForEditing(object sender, CustomRowCellEditEventArgs e)
        {
            RepositoryItemLookUpEdit LocationRepo = new RepositoryItemLookUpEdit();
            string IDs = "0";
            foreach (DataRow  row in ShipmentLocationTable.Rows )  IDs += "," + row["Location"].ToString();
            
            LocationRepo.DataSource  = Master.SqlDataTable("Select * From Distnations Where ID not in ("+IDs +") ");
            LocationRepo.ValueMember = "ID";
            LocationRepo.DisplayMember = "Name";
 
            e.RepositoryItem  = LocationRepo;
        }

        private void gridControl3_ProcessGridKey(object sender, KeyEventArgs e)
        {
            GridControl grid = sender as GridControl;
            GridView view = grid.FocusedView as GridView;
            if (e.KeyData == Keys.Delete && view.FocusedRowHandle > 0)
            {
                view.DeleteSelectedRows();
                e.Handled = true;
            }
        }

        private void gridControl4_ProcessGridKey(object sender, KeyEventArgs e)
        {
            GridControl grid = sender as GridControl;
            GridView view = grid.FocusedView as GridView;
            if (e.KeyData == Keys.Delete && view.FocusedRowHandle > 0)
            {

                if (view.GetFocusedRowCellValue("Qty") != null && view.GetFocusedRowCellValue("Qty") != DBNull.Value && (double)view.GetFocusedRowCellValue("Qty") == 0)
                {
                    view.DeleteSelectedRows();
                    e.Handled = true;
                }

            }
        }

        private void gridControl1_ProcessGridKey(object sender, KeyEventArgs e)
        {
            GridControl grid = sender as GridControl;
            GridView view = grid.FocusedView as GridView;
            if (e.KeyData == Keys.Delete && view.FocusedRowHandle >= 0)
            {
                view.DeleteSelectedRows();
                e.Handled = true;
                ShipmentOrdersTable.AcceptChanges();
                string IDs = "0";
                for (int i = 0; i < ShipmentOrdersTable.Rows.Count; i++)
                {
                    IDs += "," + "'" + ShipmentOrdersTable.Rows[i]["ID"]   + "'";
                }
                GetCurrentOrders(IDs);
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (ShipmentProducTable .Rows.Count == 0) { Master.ShowInformMessseg(this, "", "يجب ادراج صنف واحد علي الاقل"); return; }
            if (LookUpEdit_Car.Text != "خارجية")
            {
                if (ShipmentLocationTable.Rows.Count == 0) { Master.ShowInformMessseg(this, "", "يجب تحديد الواجهه"); return; }
                if (SpinEdit_Measurement_ODO.EditValue == null || Convert.ToInt64(SpinEdit_Measurement_ODO.EditValue) <= 0) { Master.ShowInformMessseg(this, "", "يجب تسجيل قراءه عداد المسافه "); return; }
                if (SpinEdit_Measurement_Fuel.EditValue == null || Convert.ToInt64(SpinEdit_Measurement_Fuel.EditValue) <= 0) { Master.ShowInformMessseg(this, "", "يجب تسجيل قراءه الوقود  "); return; }
                if (LookUpEdit_Driver .EditValue == null || Convert.ToInt64(LookUpEdit_Driver.EditValue) <= 0) { Master.ShowInformMessseg(this, "", "يجب اختيار السائق  "); return; }

            }

            SetData();
            shipment.Save();
            string SaveSQltxt  = "";
              SaveSQltxt += (" Delete from [ShipProducts] where [ShipID] = '"+shipment.ID+"'");
              SaveSQltxt += (" Delete from [ShipOrders] where [ShipID] = '" + shipment.ID + "'");
              SaveSQltxt += (" Delete from [ShipLocations] where [ShipID] = '" + shipment.ID + "'");
    
            foreach (DataRow  row in ShipmentProducTable .Rows )
                SaveSQltxt += " Insert into ShipProducts (ShipID,Product,Qty,ExtraQty)values ('"+shipment.ID + "','" + row["Product"] + "','" + row["Qty"] + "','" + row["ExtraQty"] + "') ";
            foreach (DataRow row in ShipmentOrdersTable.Rows)
                SaveSQltxt += " Insert into ShipOrders (ShipID,OrderID)values ('" + shipment.ID + "','" + row["ID"] + "' ) ";

            foreach (DataRow row in ShipmentLocationTable.Rows)
                SaveSQltxt += " Insert into ShipLocations (ShipID,Location,Distance)values ('" + shipment.ID + "','" + row["Location"] + "','" + row["Distance"] + "' ) ";
            Master.SQlCmd(SaveSQltxt);
            

            DAL.Record record_ODO = new DAL.Record();
            record_ODO.Car = shipment.Car;
            record_ODO.Type = 0;
            record_ODO.Value = SpinEdit_Measurement_ODO.Text;
            record_ODO.Date = DateEdit_Date.DateTime;
            record_ODO.Save(shipment.Measurement_ODO.ToString ());
            shipment.Measurement_ODO = record_ODO.ID;

            DAL.Record record_Fuel = new DAL.Record();
            record_Fuel.Car = shipment.Car;
            record_Fuel.Type = 1;
            record_Fuel.Value = SpinEdit_Measurement_Fuel.Text;
            record_Fuel.Date = DateEdit_Date.DateTime;
            record_Fuel.Save(shipment.Measurement_Fuel.ToString());
            shipment.Measurement_Fuel = record_Fuel.ID;

            Master.SQlCmd("Update [Order] Set Status = 3 Where ID in (Select OrderID From ShipOrders)");
            Master.SQlCmd("Update [Order] Set Status = 2 Where Status = 3 and   ID not in (Select OrderID From ShipOrders)");
            DAL.Order.SendRefresh();


            if (checkEdit1.Checked)
            {
                List<int> IdLists = new List<int>();
               
                foreach (DataRow row in ShipmentOrdersTable.Rows)
                {
                    IdLists.Add(Convert.ToInt32(row["ID"]));
                }

               frm_NewInvoices invoices =  new frm_NewInvoices(IdLists);
                invoices.LoadData();
                invoices.btn_Convert.PerformClick();
                invoices.Close();

            }
            DevExpress.XtraEditors.XtraMessageBox.Show("تم", "تم الحفظ بنجاح ", MessageBoxButtons.OK, MessageBoxIcon.Information);



        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            DataTable ProdctTableForPrint = new DataTable();
            ProdctTableForPrint.Columns.Add("Product");
            ProdctTableForPrint.Columns.Add("Qty");

           
            for (int i = 0; i < gridView4.RowCount -1 ; i++)
            {
                ProdctTableForPrint.Rows.Add();
                ProdctTableForPrint.Rows[i][0] = gridView4.GetRowCellDisplayText(i, "Product");
                ProdctTableForPrint.Rows[i][1] = gridView4.GetRowCellValue(i, "TotalQty");


            }


            Reporting.rpt_Shipment report = new Reporting.rpt_Shipment(ProdctTableForPrint, LookUpEdit_Car .Text, DateEdit_Date.Text, progressBar1.Value.ToString(), LookUpEdit_Start.Text);
            report.LoadData();

            using (ReportPrintTool printtool = new ReportPrintTool(report))
            {
                printtool.ShowRibbonPreviewDialog();
            }
        }
    }
    public static class ModifyProgressBarColor
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false)]
        static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr w, IntPtr l);
        public static void SetState(this ProgressBar pBar, int state)
        {
            SendMessage(pBar.Handle, 1040, (IntPtr)state, IntPtr.Zero);
        }
    }
}
