﻿namespace SwiftOrders.Forms
{
    partial class frm_EditOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            this.labelControl_ID = new DevExpress.XtraEditors.LabelControl();
            this.TextEdit_ID = new DevExpress.XtraEditors.TextEdit();
            this.labelControl_Clint = new DevExpress.XtraEditors.LabelControl();
            this.LookUpEdit_Clint = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl_Date = new DevExpress.XtraEditors.LabelControl();
            this.labelControl_DeliveryDate = new DevExpress.XtraEditors.LabelControl();
            this.labelControl_Car = new DevExpress.XtraEditors.LabelControl();
            this.LookUpEdit_Car = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl_OrderGroup = new DevExpress.XtraEditors.LabelControl();
            this.LookUpEdit_OrderGroup = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl_Status = new DevExpress.XtraEditors.LabelControl();
            this.LookUpEdit_Status = new DevExpress.XtraEditors.LookUpEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Save = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.DateEdit_Date = new DevExpress.XtraEditors.DateEdit();
            this.DateEdit_DeliveryDate = new DevExpress.XtraEditors.DateEdit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_ID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_Clint.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_Car.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_OrderGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_Status.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_Date.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_Date.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_DeliveryDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_DeliveryDate.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl_ID
            // 
            this.labelControl_ID.Appearance.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelControl_ID.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl_ID.Appearance.Options.UseBackColor = true;
            this.labelControl_ID.Appearance.Options.UseForeColor = true;
            this.labelControl_ID.Appearance.Options.UseTextOptions = true;
            this.labelControl_ID.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl_ID.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl_ID.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl_ID.Location = new System.Drawing.Point(217, 15);
            this.labelControl_ID.Name = "labelControl_ID";
            this.labelControl_ID.Padding = new System.Windows.Forms.Padding(5);
            this.labelControl_ID.Size = new System.Drawing.Size(96, 20);
            this.labelControl_ID.TabIndex = 1000;
            this.labelControl_ID.Text = "كود";
            // 
            // TextEdit_ID
            // 
            this.TextEdit_ID.Location = new System.Drawing.Point(8, 15);
            this.TextEdit_ID.Name = "TextEdit_ID";
            this.TextEdit_ID.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TextEdit_ID.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.TextEdit_ID.Properties.Appearance.Options.UseBackColor = true;
            this.TextEdit_ID.Properties.Appearance.Options.UseBorderColor = true;
            this.TextEdit_ID.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.TextEdit_ID.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.TextEdit_ID.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.TextEdit_ID.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.TextEdit_ID.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.TextEdit_ID.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.TextEdit_ID.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.TextEdit_ID.Size = new System.Drawing.Size(203, 20);
            this.TextEdit_ID.TabIndex = 7;
            // 
            // labelControl_Clint
            // 
            this.labelControl_Clint.Appearance.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelControl_Clint.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl_Clint.Appearance.Options.UseBackColor = true;
            this.labelControl_Clint.Appearance.Options.UseForeColor = true;
            this.labelControl_Clint.Appearance.Options.UseTextOptions = true;
            this.labelControl_Clint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl_Clint.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl_Clint.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl_Clint.Location = new System.Drawing.Point(217, 48);
            this.labelControl_Clint.Name = "labelControl_Clint";
            this.labelControl_Clint.Padding = new System.Windows.Forms.Padding(5);
            this.labelControl_Clint.Size = new System.Drawing.Size(96, 20);
            this.labelControl_Clint.TabIndex = 1000;
            this.labelControl_Clint.Text = "العميل";
            // 
            // LookUpEdit_Clint
            // 
            this.LookUpEdit_Clint.Location = new System.Drawing.Point(8, 48);
            this.LookUpEdit_Clint.Name = "LookUpEdit_Clint";
            this.LookUpEdit_Clint.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.LookUpEdit_Clint.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.LookUpEdit_Clint.Properties.Appearance.Options.UseBackColor = true;
            this.LookUpEdit_Clint.Properties.Appearance.Options.UseBorderColor = true;
            this.LookUpEdit_Clint.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.LookUpEdit_Clint.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.LookUpEdit_Clint.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.LookUpEdit_Clint.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.LookUpEdit_Clint.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.LookUpEdit_Clint.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", "Show", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.LookUpEdit_Clint.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.LookUpEdit_Clint.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.LookUpEdit_Clint.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.LookUpEdit_Clint.Properties.NullText = "";
            this.LookUpEdit_Clint.Properties.ReadOnly = true;
            this.LookUpEdit_Clint.Size = new System.Drawing.Size(203, 20);
            this.LookUpEdit_Clint.TabIndex = 7;
            // 
            // labelControl_Date
            // 
            this.labelControl_Date.Appearance.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelControl_Date.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl_Date.Appearance.Options.UseBackColor = true;
            this.labelControl_Date.Appearance.Options.UseForeColor = true;
            this.labelControl_Date.Appearance.Options.UseTextOptions = true;
            this.labelControl_Date.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl_Date.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl_Date.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl_Date.Location = new System.Drawing.Point(217, 114);
            this.labelControl_Date.Name = "labelControl_Date";
            this.labelControl_Date.Padding = new System.Windows.Forms.Padding(5);
            this.labelControl_Date.Size = new System.Drawing.Size(96, 20);
            this.labelControl_Date.TabIndex = 1000;
            this.labelControl_Date.Text = "تاريخ الطلب";
            // 
            // labelControl_DeliveryDate
            // 
            this.labelControl_DeliveryDate.Appearance.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelControl_DeliveryDate.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl_DeliveryDate.Appearance.Options.UseBackColor = true;
            this.labelControl_DeliveryDate.Appearance.Options.UseForeColor = true;
            this.labelControl_DeliveryDate.Appearance.Options.UseTextOptions = true;
            this.labelControl_DeliveryDate.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl_DeliveryDate.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl_DeliveryDate.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl_DeliveryDate.Location = new System.Drawing.Point(217, 147);
            this.labelControl_DeliveryDate.Name = "labelControl_DeliveryDate";
            this.labelControl_DeliveryDate.Padding = new System.Windows.Forms.Padding(5);
            this.labelControl_DeliveryDate.Size = new System.Drawing.Size(96, 20);
            this.labelControl_DeliveryDate.TabIndex = 1000;
            this.labelControl_DeliveryDate.Text = "موعد التسليم";
            // 
            // labelControl_Car
            // 
            this.labelControl_Car.Appearance.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelControl_Car.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl_Car.Appearance.Options.UseBackColor = true;
            this.labelControl_Car.Appearance.Options.UseForeColor = true;
            this.labelControl_Car.Appearance.Options.UseTextOptions = true;
            this.labelControl_Car.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl_Car.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl_Car.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl_Car.Location = new System.Drawing.Point(216, 15);
            this.labelControl_Car.Name = "labelControl_Car";
            this.labelControl_Car.Padding = new System.Windows.Forms.Padding(5);
            this.labelControl_Car.Size = new System.Drawing.Size(96, 20);
            this.labelControl_Car.TabIndex = 1000;
            this.labelControl_Car.Text = "السياره";
            // 
            // LookUpEdit_Car
            // 
            this.LookUpEdit_Car.Location = new System.Drawing.Point(7, 15);
            this.LookUpEdit_Car.Name = "LookUpEdit_Car";
            this.LookUpEdit_Car.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.LookUpEdit_Car.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.LookUpEdit_Car.Properties.Appearance.Options.UseBackColor = true;
            this.LookUpEdit_Car.Properties.Appearance.Options.UseBorderColor = true;
            this.LookUpEdit_Car.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.LookUpEdit_Car.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.LookUpEdit_Car.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.LookUpEdit_Car.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.LookUpEdit_Car.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.LookUpEdit_Car.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", "Show", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.LookUpEdit_Car.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.LookUpEdit_Car.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.LookUpEdit_Car.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.LookUpEdit_Car.Properties.NullText = "";
            this.LookUpEdit_Car.Size = new System.Drawing.Size(203, 20);
            this.LookUpEdit_Car.TabIndex = 7;
            this.LookUpEdit_Car.EditValueChanged += new System.EventHandler(this.LookUpEdit_Car_EditValueChanged);
            // 
            // labelControl_OrderGroup
            // 
            this.labelControl_OrderGroup.Appearance.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelControl_OrderGroup.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl_OrderGroup.Appearance.Options.UseBackColor = true;
            this.labelControl_OrderGroup.Appearance.Options.UseForeColor = true;
            this.labelControl_OrderGroup.Appearance.Options.UseTextOptions = true;
            this.labelControl_OrderGroup.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl_OrderGroup.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl_OrderGroup.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl_OrderGroup.Location = new System.Drawing.Point(217, 81);
            this.labelControl_OrderGroup.Name = "labelControl_OrderGroup";
            this.labelControl_OrderGroup.Padding = new System.Windows.Forms.Padding(5);
            this.labelControl_OrderGroup.Size = new System.Drawing.Size(96, 20);
            this.labelControl_OrderGroup.TabIndex = 1000;
            this.labelControl_OrderGroup.Text = "المجموعه";
            // 
            // LookUpEdit_OrderGroup
            // 
            this.LookUpEdit_OrderGroup.Location = new System.Drawing.Point(8, 81);
            this.LookUpEdit_OrderGroup.Name = "LookUpEdit_OrderGroup";
            this.LookUpEdit_OrderGroup.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.LookUpEdit_OrderGroup.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.LookUpEdit_OrderGroup.Properties.Appearance.Options.UseBackColor = true;
            this.LookUpEdit_OrderGroup.Properties.Appearance.Options.UseBorderColor = true;
            this.LookUpEdit_OrderGroup.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.LookUpEdit_OrderGroup.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.LookUpEdit_OrderGroup.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.LookUpEdit_OrderGroup.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.LookUpEdit_OrderGroup.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.LookUpEdit_OrderGroup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "", "Show", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.LookUpEdit_OrderGroup.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.LookUpEdit_OrderGroup.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.LookUpEdit_OrderGroup.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.LookUpEdit_OrderGroup.Properties.NullText = "";
            this.LookUpEdit_OrderGroup.Size = new System.Drawing.Size(203, 20);
            this.LookUpEdit_OrderGroup.TabIndex = 7;
            // 
            // labelControl_Status
            // 
            this.labelControl_Status.Appearance.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelControl_Status.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl_Status.Appearance.Options.UseBackColor = true;
            this.labelControl_Status.Appearance.Options.UseForeColor = true;
            this.labelControl_Status.Appearance.Options.UseTextOptions = true;
            this.labelControl_Status.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl_Status.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl_Status.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl_Status.Location = new System.Drawing.Point(216, 180);
            this.labelControl_Status.Name = "labelControl_Status";
            this.labelControl_Status.Padding = new System.Windows.Forms.Padding(5);
            this.labelControl_Status.Size = new System.Drawing.Size(96, 20);
            this.labelControl_Status.TabIndex = 1000;
            this.labelControl_Status.Text = "الحاله";
            // 
            // LookUpEdit_Status
            // 
            this.LookUpEdit_Status.Location = new System.Drawing.Point(7, 180);
            this.LookUpEdit_Status.Name = "LookUpEdit_Status";
            this.LookUpEdit_Status.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.LookUpEdit_Status.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.LookUpEdit_Status.Properties.Appearance.Options.UseBackColor = true;
            this.LookUpEdit_Status.Properties.Appearance.Options.UseBorderColor = true;
            this.LookUpEdit_Status.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.LookUpEdit_Status.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.LookUpEdit_Status.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.LookUpEdit_Status.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.LookUpEdit_Status.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.LookUpEdit_Status.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "", "Show", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.LookUpEdit_Status.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.LookUpEdit_Status.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.LookUpEdit_Status.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.LookUpEdit_Status.Properties.NullText = "";
            this.LookUpEdit_Status.Size = new System.Drawing.Size(203, 20);
            this.LookUpEdit_Status.TabIndex = 7;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Traditional Arabic", 16F, System.Drawing.FontStyle.Bold);
            this.simpleButton1.Appearance.ForeColor = System.Drawing.Color.Red;
            this.simpleButton1.Appearance.Options.UseBackColor = true;
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Appearance.Options.UseForeColor = true;
            this.simpleButton1.Location = new System.Drawing.Point(130, 447);
            this.simpleButton1.LookAndFeel.SkinName = "Foggy";
            this.simpleButton1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(109, 35);
            this.simpleButton1.TabIndex = 1005;
            this.simpleButton1.Text = "حذف";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // btn_Save
            // 
            this.btn_Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Save.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btn_Save.Appearance.Font = new System.Drawing.Font("Traditional Arabic", 16F, System.Drawing.FontStyle.Bold);
            this.btn_Save.Appearance.ForeColor = System.Drawing.Color.Green;
            this.btn_Save.Appearance.Options.UseBackColor = true;
            this.btn_Save.Appearance.Options.UseFont = true;
            this.btn_Save.Appearance.Options.UseForeColor = true;
            this.btn_Save.Location = new System.Drawing.Point(13, 447);
            this.btn_Save.LookAndFeel.SkinName = "Foggy";
            this.btn_Save.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btn_Save.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(109, 35);
            this.btn_Save.TabIndex = 1004;
            this.btn_Save.Text = "حفظ";
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            this.btn_Save.MouseEnter += new System.EventHandler(this.btn_Save_MouseEnter);
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(2);
            this.gridControl1.Location = new System.Drawing.Point(12, 227);
            this.gridControl1.LookAndFeel.SkinName = "iMaginary";
            this.gridControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(658, 210);
            this.gridControl1.TabIndex = 1003;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.gridControl1.ProcessGridKey += new System.Windows.Forms.KeyEventHandler(this.gridControl1_ProcessGridKey);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gridView1_InitNewRow);
            this.gridView1.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView1_CellValueChanged);
            this.gridView1.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.gridView1_InvalidRowException);
            this.gridView1.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridView1_ValidateRow);
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn1.Caption = "الصنف";
            this.gridColumn1.FieldName = "Product";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridControl2
            // 
            this.gridControl2.Location = new System.Drawing.Point(7, 45);
            this.gridControl2.LookAndFeel.SkinName = "Metropolis";
            this.gridControl2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(305, 159);
            this.gridControl2.TabIndex = 1006;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.ActiveFilterEnabled = false;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.Editable = false;
            this.gridView2.OptionsCustomization.AllowColumnMoving = false;
            this.gridView2.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridView2.OptionsMenu.EnableColumnMenu = false;
            this.gridView2.OptionsMenu.EnableFooterMenu = false;
            this.gridView2.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridView2.OptionsMenu.ShowAutoFilterRowItem = false;
            this.gridView2.OptionsMenu.ShowDateTimeGroupIntervalItems = false;
            this.gridView2.OptionsMenu.ShowGroupSortSummaryItems = false;
            this.gridView2.OptionsMenu.ShowSplitItem = false;
            this.gridView2.OptionsView.ShowDetailButtons = false;
            this.gridView2.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView2.OptionsView.ShowFooter = true;
            this.gridView2.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowIndicator = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.gridControl2);
            this.groupBox1.Controls.Add(this.labelControl_Car);
            this.groupBox1.Controls.Add(this.LookUpEdit_Car);
            this.groupBox1.Location = new System.Drawing.Point(12, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(322, 215);
            this.groupBox1.TabIndex = 1007;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.DateEdit_Date);
            this.groupBox2.Controls.Add(this.labelControl_ID);
            this.groupBox2.Controls.Add(this.TextEdit_ID);
            this.groupBox2.Controls.Add(this.labelControl_Clint);
            this.groupBox2.Controls.Add(this.LookUpEdit_Clint);
            this.groupBox2.Controls.Add(this.labelControl_Date);
            this.groupBox2.Controls.Add(this.labelControl_DeliveryDate);
            this.groupBox2.Controls.Add(this.DateEdit_DeliveryDate);
            this.groupBox2.Controls.Add(this.labelControl_OrderGroup);
            this.groupBox2.Controls.Add(this.LookUpEdit_OrderGroup);
            this.groupBox2.Controls.Add(this.labelControl_Status);
            this.groupBox2.Controls.Add(this.LookUpEdit_Status);
            this.groupBox2.Location = new System.Drawing.Point(348, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(322, 215);
            this.groupBox2.TabIndex = 1008;
            this.groupBox2.TabStop = false;
            // 
            // DateEdit_Date
            // 
            this.DateEdit_Date.EditValue = new System.DateTime(2018, 5, 17, 0, 0, 0, 0);
            this.DateEdit_Date.Location = new System.Drawing.Point(8, 114);
            this.DateEdit_Date.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.DateEdit_Date.Name = "DateEdit_Date";
            this.DateEdit_Date.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.DateEdit_Date.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.DateEdit_Date.Properties.Appearance.Options.UseBackColor = true;
            this.DateEdit_Date.Properties.Appearance.Options.UseBorderColor = true;
            this.DateEdit_Date.Properties.Appearance.Options.UseTextOptions = true;
            this.DateEdit_Date.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DateEdit_Date.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DateEdit_Date.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.DateEdit_Date.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.DateEdit_Date.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.DateEdit_Date.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.DateEdit_Date.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.DateEdit_Date.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEdit_Date.Properties.DisplayFormat.FormatString = "yyyy-MM-dd hh:mm tt";
            this.DateEdit_Date.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.DateEdit_Date.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.DateEdit_Date.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.DateEdit_Date.Properties.ReadOnly = true;
            this.DateEdit_Date.Size = new System.Drawing.Size(203, 20);
            this.DateEdit_Date.TabIndex = 1001;
            // 
            // DateEdit_DeliveryDate
            // 
            this.DateEdit_DeliveryDate.EditValue = new System.DateTime(2018, 5, 29, 0, 0, 0, 0);
            this.DateEdit_DeliveryDate.Location = new System.Drawing.Point(8, 147);
            this.DateEdit_DeliveryDate.Name = "DateEdit_DeliveryDate";
            this.DateEdit_DeliveryDate.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.DateEdit_DeliveryDate.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.DateEdit_DeliveryDate.Properties.Appearance.Options.UseBackColor = true;
            this.DateEdit_DeliveryDate.Properties.Appearance.Options.UseBorderColor = true;
            this.DateEdit_DeliveryDate.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.DateEdit_DeliveryDate.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.DateEdit_DeliveryDate.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.DateEdit_DeliveryDate.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.DateEdit_DeliveryDate.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.DateEdit_DeliveryDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Down)});
            this.DateEdit_DeliveryDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEdit_DeliveryDate.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.DateEdit_DeliveryDate.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.DateEdit_DeliveryDate.Size = new System.Drawing.Size(203, 20);
            this.DateEdit_DeliveryDate.TabIndex = 7;
            // 
            // frm_EditOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(678, 496);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.btn_Save);
            this.Controls.Add(this.gridControl1);
            this.KeyPreview = true;
            this.Name = "frm_EditOrder";
            this.Text = "التعديل علي الطليبه";
            this.Load += new System.EventHandler(this.frm_EditOrder_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frm_EditOrder_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_ID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_Clint.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_Car.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_OrderGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_Status.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_Date.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_Date.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_DeliveryDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_DeliveryDate.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        // Genreated By ClassMaker
        private DevExpress.XtraEditors.TextEdit TextEdit_ID;
        DevExpress.XtraEditors.LabelControl labelControl_ID;
        private DevExpress.XtraEditors.LookUpEdit LookUpEdit_Clint;
        DevExpress.XtraEditors.LabelControl labelControl_Clint;
        DevExpress.XtraEditors.LabelControl labelControl_Date;
        DevExpress.XtraEditors.LabelControl labelControl_DeliveryDate;
        private DevExpress.XtraEditors.LookUpEdit LookUpEdit_Car;
        DevExpress.XtraEditors.LabelControl labelControl_Car;
        private DevExpress.XtraEditors.LookUpEdit LookUpEdit_OrderGroup;
        DevExpress.XtraEditors.LabelControl labelControl_OrderGroup;
        private DevExpress.XtraEditors.LookUpEdit LookUpEdit_Status;
        DevExpress.XtraEditors.LabelControl labelControl_Status;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton btn_Save;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevExpress.XtraEditors.DateEdit DateEdit_Date;
        private DevExpress.XtraEditors.DateEdit DateEdit_DeliveryDate;
    }
}