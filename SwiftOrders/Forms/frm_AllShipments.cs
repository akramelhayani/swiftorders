﻿using DevExpress.XtraEditors.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SwiftOrders.Forms
{
    public partial class frm_AllShipments : DevExpress.XtraEditors.XtraForm 
    {
        public frm_AllShipments()
        {
            InitializeComponent();
        }

        private void frm_AllShipments_Load(object sender, EventArgs e)
        {
            RefreshDetails();
            IntGridView();

        }
        void IntGridView()
        {
            gridView1.Columns["ID"].Caption = "كود";
            gridView1.Columns["Date"].Caption = "التاريخ";
 
            gridView1.Columns["Distnce"].Caption = "المسافه";
            gridView1.Columns["Weight"].Caption = "الوزن";
            gridView1.Columns["Car"].Caption = "السياره";
            gridView1.Columns["Driver"].Caption = "السائق";
            gridView1.Columns["Measurement_ODO"].Caption = "قراءه عداد المسافه";
            gridView1.Columns["Measurement_Fuel"].Caption = "استهلاك الوقود السابق";

            RepositoryItemLookUpEdit repo = new RepositoryItemLookUpEdit();
            repo.DataSource = Master.SqlDataTable("Select * FROM drivers");
            repo.ValueMember = "ID";
            repo.DisplayMember  = "Name";
            gridControl1.RepositoryItems.Add(repo);
            gridView1.Columns["Driver"].ColumnEdit = repo;

            RepositoryItemLookUpEdit repoODO = new RepositoryItemLookUpEdit();
            repoODO.DataSource = Master.SqlDataTable("Select ID ,Value FROM [Car_Record] where [Type] = 0 ");
            repoODO.ValueMember = "ID";
            repoODO.DisplayMember = "Value";
            gridControl1.RepositoryItems.Add(repoODO);
            gridView1.Columns["Measurement_ODO"].ColumnEdit = repoODO;

            RepositoryItemLookUpEdit repoFUEL = new RepositoryItemLookUpEdit();
            repoFUEL.DataSource = Master.SqlDataTable("Select ID ,Value FROM [Car_Record] where [Type] = 1 ");
            repoFUEL.ValueMember = "ID";
            repoFUEL.DisplayMember = "Value";
            gridControl1.RepositoryItems.Add(repoFUEL);
            gridView1.Columns["Measurement_Fuel"].ColumnEdit = repoFUEL;


            for (int i = 0; i < gridView1.Columns.Count -1; i++)
            {
                gridView1.Columns[i].BestFit();
                gridView1.Columns[i].AppearanceCell.TextOptions.HAlignment  = DevExpress.Utils.HorzAlignment.Near  ;

            }

        }
        public void RefreshDetails()
        {
            DataTable table = new DataTable();
            string sqlTXT = "SELECT [ID]                                                                         "
     + "   ,[Car]                                                                                               "
     + "                                                                                                 "
     + "  , ISNULL( (Select SUM([Distance]) from ShipLocations where [ShipID] = t.ID ) ,0)as 'Distnce'           "
     + "  ,ISNULL((SELECT  SUM([Qty] +[ExtraQty])FROM[ShipProducts] where[ShipID] = t.ID ) ,0)as 'Weight'        "
     + "   ,[Date]                                                                                                "
     + "  ,[Driver]                                                                                              "
     + "  ,[Measurement_ODO]                                                                                     "
     + "  ,[Measurement_Fuel]                                                                                    "
     + "    FROM[OMDB].[dbo].[Shipment] as t ";
            table = Master.SqlDataTable(sqlTXT);
            gridControl1.DataSource = table;
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            if(gridView1.FocusedRowHandle >= 0)
            {
                new frm_Shipment((int) gridView1.GetFocusedRowCellValue("ID")).Show();
            }
        }
    }
}
