﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SwiftOrders.Forms
{
    public partial class frm_LogCarRecord : DevExpress.XtraEditors.XtraForm 
    {
        DAL.Record record = new DAL.Record();
        public frm_LogCarRecord()
        {
            InitializeComponent();
        }

        private void frm_LogCarRecord_Load(object sender, EventArgs e)
        {
            DAL.Car.ToLookUp(LookUpEdit_Car);
            DateEdit_Date.EditValue = DateTime.Now;
            New();
            simpleButton1.Click += simpleButton1_Click;
            LookUpEdit_Type.EditValueChanged += LookUpEdit_Car_EditValueChanged;

        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (!(LookUpEdit_Car.EditValue is int)) return;
            SetData();
            record.Save();
            Master.ShowInformMessseg(this, "تم", "تم الحفط بنجاح");
        }
        public void SetData()
        {
         
            record.Car =(int) LookUpEdit_Car.EditValue;
            record.Type = LookUpEdit_Type.SelectedIndex;
            record.Value = TextEdit_Value.Text;
            record.Date = DateEdit_Date.DateTime;
        }
        public void GetData()
        {
           
            LookUpEdit_Car.EditValue = (Int32)record.Car;
            LookUpEdit_Type.SelectedIndex = (Int32)record.Type;
            TextEdit_Value.Text = (string)record.Value;
            DateEdit_Date.DateTime = (DateTime)record.Date;
        }
        public   void New()
        {
           
            record = new DAL.Record();
            GetData();
           
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            New();
        }

        string FindPrevious(string car , int type)
        {
            DataTable dt =  Master.SqlDataTable("  select top 1 [Value] FROM  [Car_Record] where Car = '"+car+"' and Type = '"+type+"'  order by [Date] DESC ");
            if (dt.Rows.Count == 1) return dt.Rows[0][0].ToString();
            return "";

        }

        private void LookUpEdit_Car_EditValueChanged(object sender, EventArgs e)
        {
            textEdit1.Text = FindPrevious(LookUpEdit_Car.EditValue.ToString(), LookUpEdit_Type.SelectedIndex);
        }

        private void simpleButton1_Click_1(object sender, EventArgs e)
        {

        }
    }
}
