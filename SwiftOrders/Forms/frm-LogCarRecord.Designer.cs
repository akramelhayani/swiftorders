﻿namespace SwiftOrders.Forms
{
    partial class frm_LogCarRecord
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            this.labelControl_Car = new DevExpress.XtraEditors.LabelControl();
            this.LookUpEdit_Car = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl_Type = new DevExpress.XtraEditors.LabelControl();
            this.labelControl_Value = new DevExpress.XtraEditors.LabelControl();
            this.TextEdit_Value = new DevExpress.XtraEditors.TextEdit();
            this.labelControl_Date = new DevExpress.XtraEditors.LabelControl();
            this.DateEdit_Date = new DevExpress.XtraEditors.DateEdit();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.LookUpEdit_Type = new DevExpress.XtraEditors.ComboBoxEdit();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_Car.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_Value.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_Date.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_Date.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_Type.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl_Car
            // 
            this.labelControl_Car.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl_Car.Appearance.Options.UseForeColor = true;
            this.labelControl_Car.Location = new System.Drawing.Point(215, 15);
            this.labelControl_Car.Name = "labelControl_Car";
            this.labelControl_Car.Size = new System.Drawing.Size(34, 13);
            this.labelControl_Car.TabIndex = 1000;
            this.labelControl_Car.Text = "السياره";
            // 
            // LookUpEdit_Car
            // 
            this.LookUpEdit_Car.Location = new System.Drawing.Point(11, 12);
            this.LookUpEdit_Car.Name = "LookUpEdit_Car";
            this.LookUpEdit_Car.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.LookUpEdit_Car.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.LookUpEdit_Car.Properties.Appearance.Options.UseBackColor = true;
            this.LookUpEdit_Car.Properties.Appearance.Options.UseBorderColor = true;
            this.LookUpEdit_Car.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.LookUpEdit_Car.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.LookUpEdit_Car.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.LookUpEdit_Car.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.LookUpEdit_Car.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.LookUpEdit_Car.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", "Show", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.LookUpEdit_Car.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.LookUpEdit_Car.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.LookUpEdit_Car.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.LookUpEdit_Car.Properties.NullText = "";
            this.LookUpEdit_Car.Size = new System.Drawing.Size(161, 20);
            this.LookUpEdit_Car.TabIndex = 7;
            this.LookUpEdit_Car.EditValueChanged += new System.EventHandler(this.LookUpEdit_Car_EditValueChanged);
            // 
            // labelControl_Type
            // 
            this.labelControl_Type.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl_Type.Appearance.Options.UseForeColor = true;
            this.labelControl_Type.Location = new System.Drawing.Point(199, 45);
            this.labelControl_Type.Name = "labelControl_Type";
            this.labelControl_Type.Size = new System.Drawing.Size(50, 13);
            this.labelControl_Type.TabIndex = 1000;
            this.labelControl_Type.Text = "نوع القراءه ";
            // 
            // labelControl_Value
            // 
            this.labelControl_Value.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl_Value.Appearance.Options.UseForeColor = true;
            this.labelControl_Value.Location = new System.Drawing.Point(220, 97);
            this.labelControl_Value.Name = "labelControl_Value";
            this.labelControl_Value.Size = new System.Drawing.Size(29, 13);
            this.labelControl_Value.TabIndex = 1000;
            this.labelControl_Value.Text = "القراءه";
            // 
            // TextEdit_Value
            // 
            this.TextEdit_Value.Location = new System.Drawing.Point(11, 94);
            this.TextEdit_Value.Name = "TextEdit_Value";
            this.TextEdit_Value.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TextEdit_Value.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.TextEdit_Value.Properties.Appearance.Options.UseBackColor = true;
            this.TextEdit_Value.Properties.Appearance.Options.UseBorderColor = true;
            this.TextEdit_Value.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.TextEdit_Value.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.TextEdit_Value.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.TextEdit_Value.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.TextEdit_Value.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.TextEdit_Value.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.TextEdit_Value.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.TextEdit_Value.Size = new System.Drawing.Size(161, 20);
            this.TextEdit_Value.TabIndex = 7;
            // 
            // labelControl_Date
            // 
            this.labelControl_Date.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl_Date.Appearance.Options.UseForeColor = true;
            this.labelControl_Date.Location = new System.Drawing.Point(221, 71);
            this.labelControl_Date.Name = "labelControl_Date";
            this.labelControl_Date.Size = new System.Drawing.Size(28, 13);
            this.labelControl_Date.TabIndex = 1000;
            this.labelControl_Date.Text = "التاريخ";
            // 
            // DateEdit_Date
            // 
            this.DateEdit_Date.EditValue = new System.DateTime(2018, 6, 13, 0, 0, 0, 0);
            this.DateEdit_Date.Location = new System.Drawing.Point(11, 68);
            this.DateEdit_Date.Name = "DateEdit_Date";
            this.DateEdit_Date.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.DateEdit_Date.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.DateEdit_Date.Properties.Appearance.Options.UseBackColor = true;
            this.DateEdit_Date.Properties.Appearance.Options.UseBorderColor = true;
            this.DateEdit_Date.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.DateEdit_Date.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.DateEdit_Date.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.DateEdit_Date.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.DateEdit_Date.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.DateEdit_Date.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEdit_Date.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.DateEdit_Date.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.DateEdit_Date.Size = new System.Drawing.Size(161, 20);
            this.DateEdit_Date.TabIndex = 7;
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(11, 120);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textEdit1.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.textEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit1.Properties.Appearance.Options.UseBorderColor = true;
            this.textEdit1.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.textEdit1.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.textEdit1.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.textEdit1.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.textEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.textEdit1.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.textEdit1.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.textEdit1.Size = new System.Drawing.Size(161, 20);
            this.textEdit1.TabIndex = 7;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Location = new System.Drawing.Point(178, 123);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(71, 13);
            this.labelControl1.TabIndex = 1000;
            this.labelControl1.Text = "القراءه السابقه ";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(11, 151);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 1001;
            this.simpleButton1.Text = "تسجيل";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // LookUpEdit_Type
            // 
            this.LookUpEdit_Type.EditValue = "قراءه عداد المسافه";
            this.LookUpEdit_Type.Location = new System.Drawing.Point(11, 42);
            this.LookUpEdit_Type.Name = "LookUpEdit_Type";
            this.LookUpEdit_Type.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.LookUpEdit_Type.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.LookUpEdit_Type.Properties.Appearance.Options.UseBackColor = true;
            this.LookUpEdit_Type.Properties.Appearance.Options.UseBorderColor = true;
            this.LookUpEdit_Type.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.LookUpEdit_Type.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.LookUpEdit_Type.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.LookUpEdit_Type.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.LookUpEdit_Type.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.LookUpEdit_Type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", "Show", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.LookUpEdit_Type.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.LookUpEdit_Type.Properties.Items.AddRange(new object[] {
            "قراءه عداد المسافه",
            " استهلاك كميه وقود"});
            this.LookUpEdit_Type.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.LookUpEdit_Type.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.LookUpEdit_Type.Properties.PopupSizeable = true;
            this.LookUpEdit_Type.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.LookUpEdit_Type.Size = new System.Drawing.Size(161, 20);
            this.LookUpEdit_Type.TabIndex = 7;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(93, 151);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 1002;
            this.simpleButton2.Text = "جديد";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(174, 151);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(75, 23);
            this.simpleButton3.TabIndex = 1002;
            this.simpleButton3.Text = "حذف";
            // 
            // frm_LogCarRecord
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(270, 186);
            this.Controls.Add(this.simpleButton3);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.labelControl_Car);
            this.Controls.Add(this.LookUpEdit_Car);
            this.Controls.Add(this.labelControl_Type);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.labelControl_Value);
            this.Controls.Add(this.textEdit1);
            this.Controls.Add(this.TextEdit_Value);
            this.Controls.Add(this.labelControl_Date);
            this.Controls.Add(this.DateEdit_Date);
            this.Controls.Add(this.LookUpEdit_Type);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frm_LogCarRecord";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "قراءه عداد";
            this.Load += new System.EventHandler(this.frm_LogCarRecord_Load);
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_Car.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_Value.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_Date.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_Date.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_Type.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraEditors.LookUpEdit LookUpEdit_Car;
        DevExpress.XtraEditors.LabelControl labelControl_Car;
        DevExpress.XtraEditors.LabelControl labelControl_Type;
        private DevExpress.XtraEditors.TextEdit TextEdit_Value;
        DevExpress.XtraEditors.LabelControl labelControl_Value;
        private DevExpress.XtraEditors.DateEdit DateEdit_Date;
        DevExpress.XtraEditors.LabelControl labelControl_Date;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.ComboBoxEdit LookUpEdit_Type;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
    }
}