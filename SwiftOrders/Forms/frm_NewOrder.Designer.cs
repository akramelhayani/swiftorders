﻿namespace SwiftOrders.Forms
{
    partial class frm_NewOrder
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            this.TextEdit_ID = new DevExpress.XtraEditors.TextEdit();
            this.labelControl_Clint = new DevExpress.XtraEditors.LabelControl();
            this.SearchLookUpEdit_Clint = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.labelControl_Date = new DevExpress.XtraEditors.LabelControl();
            this.DateEdit_Date = new DevExpress.XtraEditors.DateEdit();
            this.labelControl_OrderGroup = new DevExpress.XtraEditors.LabelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpEdit_OrderGroup = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btn_Save = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_ID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SearchLookUpEdit_Clint.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_Date.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_Date.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_OrderGroup.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // TextEdit_ID
            // 
            this.TextEdit_ID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_ID.Location = new System.Drawing.Point(625, 37);
            this.TextEdit_ID.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TextEdit_ID.Name = "TextEdit_ID";
            this.TextEdit_ID.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TextEdit_ID.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.TextEdit_ID.Properties.Appearance.Options.UseBackColor = true;
            this.TextEdit_ID.Properties.Appearance.Options.UseBorderColor = true;
            this.TextEdit_ID.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.TextEdit_ID.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.TextEdit_ID.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.TextEdit_ID.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.TextEdit_ID.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.TextEdit_ID.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.TextEdit_ID.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.TextEdit_ID.Size = new System.Drawing.Size(104, 20);
            this.TextEdit_ID.TabIndex = 7;
            // 
            // labelControl_Clint
            // 
            this.labelControl_Clint.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl_Clint.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.labelControl_Clint.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.labelControl_Clint.Appearance.BorderColor = System.Drawing.Color.Gray;
            this.labelControl_Clint.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl_Clint.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl_Clint.Appearance.Options.UseBackColor = true;
            this.labelControl_Clint.Appearance.Options.UseBorderColor = true;
            this.labelControl_Clint.Appearance.Options.UseForeColor = true;
            this.labelControl_Clint.Appearance.Options.UseTextOptions = true;
            this.labelControl_Clint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl_Clint.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl_Clint.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl_Clint.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl_Clint.Location = new System.Drawing.Point(435, 5);
            this.labelControl_Clint.LookAndFeel.SkinName = "Seven Classic";
            this.labelControl_Clint.LookAndFeel.UseDefaultLookAndFeel = false;
            this.labelControl_Clint.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.labelControl_Clint.Name = "labelControl_Clint";
            this.labelControl_Clint.Size = new System.Drawing.Size(181, 31);
            this.labelControl_Clint.TabIndex = 1000;
            this.labelControl_Clint.Text = "العميل";
            // 
            // SearchLookUpEdit_Clint
            // 
            this.SearchLookUpEdit_Clint.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SearchLookUpEdit_Clint.EditValue = "";
            this.SearchLookUpEdit_Clint.Location = new System.Drawing.Point(435, 37);
            this.SearchLookUpEdit_Clint.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.SearchLookUpEdit_Clint.Name = "SearchLookUpEdit_Clint";
            this.SearchLookUpEdit_Clint.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.SearchLookUpEdit_Clint.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.SearchLookUpEdit_Clint.Properties.Appearance.Options.UseBackColor = true;
            this.SearchLookUpEdit_Clint.Properties.Appearance.Options.UseBorderColor = true;
            this.SearchLookUpEdit_Clint.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.SearchLookUpEdit_Clint.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.SearchLookUpEdit_Clint.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.SearchLookUpEdit_Clint.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.SearchLookUpEdit_Clint.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.SearchLookUpEdit_Clint.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.SearchLookUpEdit_Clint.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.SearchLookUpEdit_Clint.Properties.NullText = "";
            this.SearchLookUpEdit_Clint.Size = new System.Drawing.Size(181, 20);
            this.SearchLookUpEdit_Clint.TabIndex = 7;
            // 
            // labelControl_Date
            // 
            this.labelControl_Date.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.labelControl_Date.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.labelControl_Date.Appearance.BorderColor = System.Drawing.Color.Gray;
            this.labelControl_Date.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl_Date.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl_Date.Appearance.Options.UseBackColor = true;
            this.labelControl_Date.Appearance.Options.UseBorderColor = true;
            this.labelControl_Date.Appearance.Options.UseForeColor = true;
            this.labelControl_Date.Appearance.Options.UseTextOptions = true;
            this.labelControl_Date.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl_Date.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl_Date.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl_Date.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl_Date.Location = new System.Drawing.Point(165, 5);
            this.labelControl_Date.LookAndFeel.SkinName = "Seven Classic";
            this.labelControl_Date.LookAndFeel.UseDefaultLookAndFeel = false;
            this.labelControl_Date.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.labelControl_Date.Name = "labelControl_Date";
            this.labelControl_Date.Size = new System.Drawing.Size(261, 31);
            this.labelControl_Date.TabIndex = 1000;
            this.labelControl_Date.Text = "التاريخ";
            // 
            // DateEdit_Date
            // 
            this.DateEdit_Date.EditValue = new System.DateTime(2018, 5, 17, 0, 0, 0, 0);
            this.DateEdit_Date.Location = new System.Drawing.Point(165, 37);
            this.DateEdit_Date.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.DateEdit_Date.Name = "DateEdit_Date";
            this.DateEdit_Date.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.DateEdit_Date.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.DateEdit_Date.Properties.Appearance.Options.UseBackColor = true;
            this.DateEdit_Date.Properties.Appearance.Options.UseBorderColor = true;
            this.DateEdit_Date.Properties.Appearance.Options.UseTextOptions = true;
            this.DateEdit_Date.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DateEdit_Date.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DateEdit_Date.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.DateEdit_Date.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.DateEdit_Date.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.DateEdit_Date.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.DateEdit_Date.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.DateEdit_Date.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEdit_Date.Properties.DisplayFormat.FormatString = "yyyy-MM-dd hh:mm tt";
            this.DateEdit_Date.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.DateEdit_Date.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.DateEdit_Date.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.DateEdit_Date.Properties.ReadOnly = true;
            this.DateEdit_Date.Size = new System.Drawing.Size(261, 20);
            this.DateEdit_Date.TabIndex = 7;
            // 
            // labelControl_OrderGroup
            // 
            this.labelControl_OrderGroup.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.labelControl_OrderGroup.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.labelControl_OrderGroup.Appearance.BorderColor = System.Drawing.Color.Gray;
            this.labelControl_OrderGroup.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl_OrderGroup.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl_OrderGroup.Appearance.Options.UseBackColor = true;
            this.labelControl_OrderGroup.Appearance.Options.UseBorderColor = true;
            this.labelControl_OrderGroup.Appearance.Options.UseForeColor = true;
            this.labelControl_OrderGroup.Appearance.Options.UseTextOptions = true;
            this.labelControl_OrderGroup.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl_OrderGroup.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl_OrderGroup.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl_OrderGroup.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl_OrderGroup.Location = new System.Drawing.Point(6, 5);
            this.labelControl_OrderGroup.LookAndFeel.SkinName = "Seven Classic";
            this.labelControl_OrderGroup.LookAndFeel.UseDefaultLookAndFeel = false;
            this.labelControl_OrderGroup.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.labelControl_OrderGroup.Name = "labelControl_OrderGroup";
            this.labelControl_OrderGroup.Size = new System.Drawing.Size(150, 31);
            this.labelControl_OrderGroup.TabIndex = 1000;
            this.labelControl_OrderGroup.Text = "المجموعه";
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(2);
            this.gridControl1.Location = new System.Drawing.Point(4, 77);
            this.gridControl1.LookAndFeel.SkinName = "iMaginary";
            this.gridControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(725, 321);
            this.gridControl1.TabIndex = 1001;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gridView1_InitNewRow);
            this.gridView1.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView1_CellValueChanged);
            this.gridView1.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView1_CellValueChanging);
            this.gridView1.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.gridView1_InvalidRowException);
            this.gridView1.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridView1_ValidateRow);
            this.gridView1.InvalidValueException += new DevExpress.XtraEditors.Controls.InvalidValueExceptionEventHandler(this.gridView1_InvalidValueException);
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn1.Caption = "الصنف";
            this.gridColumn1.FieldName = "Product";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // LookUpEdit_OrderGroup
            // 
            this.LookUpEdit_OrderGroup.Location = new System.Drawing.Point(6, 37);
            this.LookUpEdit_OrderGroup.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.LookUpEdit_OrderGroup.Name = "LookUpEdit_OrderGroup";
            this.LookUpEdit_OrderGroup.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.LookUpEdit_OrderGroup.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.LookUpEdit_OrderGroup.Properties.Appearance.Options.UseBackColor = true;
            this.LookUpEdit_OrderGroup.Properties.Appearance.Options.UseBorderColor = true;
            this.LookUpEdit_OrderGroup.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.LookUpEdit_OrderGroup.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.LookUpEdit_OrderGroup.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.LookUpEdit_OrderGroup.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.LookUpEdit_OrderGroup.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.LookUpEdit_OrderGroup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", "Show", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.LookUpEdit_OrderGroup.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.LookUpEdit_OrderGroup.Properties.Items.AddRange(new object[] {
            "المصنع",
            "ميت غمر ",
            "اخري"});
            this.LookUpEdit_OrderGroup.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.LookUpEdit_OrderGroup.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.LookUpEdit_OrderGroup.Properties.PopupSizeable = true;
            this.LookUpEdit_OrderGroup.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.LookUpEdit_OrderGroup.Size = new System.Drawing.Size(150, 20);
            this.LookUpEdit_OrderGroup.TabIndex = 7;
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.labelControl1.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.labelControl1.Appearance.BorderColor = System.Drawing.Color.Gray;
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl1.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl1.Appearance.Options.UseBackColor = true;
            this.labelControl1.Appearance.Options.UseBorderColor = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Appearance.Options.UseTextOptions = true;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl1.Location = new System.Drawing.Point(625, 5);
            this.labelControl1.LookAndFeel.SkinName = "Seven Classic";
            this.labelControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(104, 31);
            this.labelControl1.TabIndex = 1000;
            this.labelControl1.Text = "كود";
            // 
            // btn_Save
            // 
            this.btn_Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_Save.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btn_Save.Appearance.Font = new System.Drawing.Font("Traditional Arabic", 16F, System.Drawing.FontStyle.Bold);
            this.btn_Save.Appearance.ForeColor = System.Drawing.Color.Green;
            this.btn_Save.Appearance.Options.UseBackColor = true;
            this.btn_Save.Appearance.Options.UseFont = true;
            this.btn_Save.Appearance.Options.UseForeColor = true;
            this.btn_Save.Location = new System.Drawing.Point(5, 408);
            this.btn_Save.LookAndFeel.SkinName = "Foggy";
            this.btn_Save.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btn_Save.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(79, 35);
            this.btn_Save.TabIndex = 1002;
            this.btn_Save.Text = "حفظ";
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            this.btn_Save.MouseEnter += new System.EventHandler(this.btn_Save_MouseEnter);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButton1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Traditional Arabic", 16F, System.Drawing.FontStyle.Bold);
            this.simpleButton1.Appearance.ForeColor = System.Drawing.Color.Red;
            this.simpleButton1.Appearance.Options.UseBackColor = true;
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Appearance.Options.UseForeColor = true;
            this.simpleButton1.Location = new System.Drawing.Point(92, 408);
            this.simpleButton1.LookAndFeel.SkinName = "Foggy";
            this.simpleButton1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(79, 35);
            this.simpleButton1.TabIndex = 1002;
            this.simpleButton1.Text = "الغاء";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // frm_NewOrder
            // 
            this.ClientSize = new System.Drawing.Size(742, 447);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.btn_Save);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.TextEdit_ID);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.labelControl_Clint);
            this.Controls.Add(this.SearchLookUpEdit_Clint);
            this.Controls.Add(this.labelControl_Date);
            this.Controls.Add(this.DateEdit_Date);
            this.Controls.Add(this.labelControl_OrderGroup);
            this.Controls.Add(this.LookUpEdit_OrderGroup);
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frm_NewOrder";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Text = "طلبيه جديده";
            this.Load += new System.EventHandler(this.ctrl_NewOrder_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frm_NewOrder_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_ID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SearchLookUpEdit_Clint.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_Date.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_Date.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_OrderGroup.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        // Genreated By ClassMaker
        private DevExpress.XtraEditors.TextEdit TextEdit_ID;
        private DevExpress.XtraEditors.SearchLookUpEdit SearchLookUpEdit_Clint;
        DevExpress.XtraEditors.LabelControl labelControl_Clint;
        private DevExpress.XtraEditors.DateEdit DateEdit_Date;
        DevExpress.XtraEditors.LabelControl labelControl_Date;
        DevExpress.XtraEditors.LabelControl labelControl_OrderGroup;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btn_Save;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.ComboBoxEdit LookUpEdit_OrderGroup;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
    }
}
