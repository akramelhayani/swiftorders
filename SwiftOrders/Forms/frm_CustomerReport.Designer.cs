﻿namespace SwiftOrders.Forms
{
    partial class frm_CustomerReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl_Clint = new DevExpress.XtraEditors.LabelControl();
            this.SearchLookUpEdit_Clint = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.labelControl_Date = new DevExpress.XtraEditors.LabelControl();
            this.DateEdit_From = new DevExpress.XtraEditors.DateEdit();
            this.DateEDIT_To = new DevExpress.XtraEditors.DateEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btn_Save = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.spinEdit1 = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            ((System.ComponentModel.ISupportInitialize)(this.SearchLookUpEdit_Clint.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_From.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_From.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEDIT_To.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEDIT_To.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl_Clint
            // 
            this.labelControl_Clint.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl_Clint.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.labelControl_Clint.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.labelControl_Clint.Appearance.BorderColor = System.Drawing.Color.Gray;
            this.labelControl_Clint.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl_Clint.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl_Clint.Appearance.Options.UseBackColor = true;
            this.labelControl_Clint.Appearance.Options.UseBorderColor = true;
            this.labelControl_Clint.Appearance.Options.UseForeColor = true;
            this.labelControl_Clint.Appearance.Options.UseTextOptions = true;
            this.labelControl_Clint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl_Clint.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl_Clint.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl_Clint.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl_Clint.Location = new System.Drawing.Point(13, 14);
            this.labelControl_Clint.LookAndFeel.SkinName = "Seven Classic";
            this.labelControl_Clint.LookAndFeel.UseDefaultLookAndFeel = false;
            this.labelControl_Clint.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.labelControl_Clint.Name = "labelControl_Clint";
            this.labelControl_Clint.Size = new System.Drawing.Size(531, 31);
            this.labelControl_Clint.TabIndex = 1002;
            this.labelControl_Clint.Text = "العميل";
            // 
            // SearchLookUpEdit_Clint
            // 
            this.SearchLookUpEdit_Clint.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SearchLookUpEdit_Clint.EditValue = "";
            this.SearchLookUpEdit_Clint.Location = new System.Drawing.Point(13, 46);
            this.SearchLookUpEdit_Clint.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.SearchLookUpEdit_Clint.Name = "SearchLookUpEdit_Clint";
            this.SearchLookUpEdit_Clint.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.SearchLookUpEdit_Clint.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.SearchLookUpEdit_Clint.Properties.Appearance.Options.UseBackColor = true;
            this.SearchLookUpEdit_Clint.Properties.Appearance.Options.UseBorderColor = true;
            this.SearchLookUpEdit_Clint.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.SearchLookUpEdit_Clint.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.SearchLookUpEdit_Clint.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.SearchLookUpEdit_Clint.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.SearchLookUpEdit_Clint.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.SearchLookUpEdit_Clint.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.SearchLookUpEdit_Clint.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.SearchLookUpEdit_Clint.Properties.NullText = "";
            this.SearchLookUpEdit_Clint.Size = new System.Drawing.Size(531, 20);
            this.SearchLookUpEdit_Clint.TabIndex = 1001;
            // 
            // labelControl_Date
            // 
            this.labelControl_Date.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.labelControl_Date.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.labelControl_Date.Appearance.BorderColor = System.Drawing.Color.Gray;
            this.labelControl_Date.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl_Date.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl_Date.Appearance.Options.UseBackColor = true;
            this.labelControl_Date.Appearance.Options.UseBorderColor = true;
            this.labelControl_Date.Appearance.Options.UseForeColor = true;
            this.labelControl_Date.Appearance.Options.UseTextOptions = true;
            this.labelControl_Date.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl_Date.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl_Date.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl_Date.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl_Date.Location = new System.Drawing.Point(13, 76);
            this.labelControl_Date.LookAndFeel.SkinName = "Seven Classic";
            this.labelControl_Date.LookAndFeel.UseDefaultLookAndFeel = false;
            this.labelControl_Date.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.labelControl_Date.Name = "labelControl_Date";
            this.labelControl_Date.Size = new System.Drawing.Size(171, 31);
            this.labelControl_Date.TabIndex = 1004;
            this.labelControl_Date.Text = "حتي التاريخ ";
            // 
            // DateEdit_From
            // 
            this.DateEdit_From.EditValue = new System.DateTime(2018, 5, 17, 0, 0, 0, 0);
            this.DateEdit_From.Location = new System.Drawing.Point(193, 108);
            this.DateEdit_From.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.DateEdit_From.Name = "DateEdit_From";
            this.DateEdit_From.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.DateEdit_From.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.DateEdit_From.Properties.Appearance.Options.UseBackColor = true;
            this.DateEdit_From.Properties.Appearance.Options.UseBorderColor = true;
            this.DateEdit_From.Properties.Appearance.Options.UseTextOptions = true;
            this.DateEdit_From.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DateEdit_From.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DateEdit_From.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.DateEdit_From.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.DateEdit_From.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.DateEdit_From.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.DateEdit_From.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.DateEdit_From.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEdit_From.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.DateEdit_From.Properties.EditFormat .FormatString = "yyyy-MM-dd";

            this.DateEdit_From.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.DateEdit_From.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEdit_From.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEdit_From.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.DateEdit_From.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.DateEdit_From.Size = new System.Drawing.Size(171, 20);
            this.DateEdit_From.TabIndex = 1003;
            // 
            // DateEDIT_To
            // 
            this.DateEDIT_To.EditValue = new System.DateTime(2018, 5, 17, 0, 0, 0, 0);
            this.DateEDIT_To.Location = new System.Drawing.Point(13, 108);
            this.DateEDIT_To.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.DateEDIT_To.Name = "DateEDIT_To";
            this.DateEDIT_To.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.DateEDIT_To.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.DateEDIT_To.Properties.Appearance.Options.UseBackColor = true;
            this.DateEDIT_To.Properties.Appearance.Options.UseBorderColor = true;
            this.DateEDIT_To.Properties.Appearance.Options.UseTextOptions = true;
            this.DateEDIT_To.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DateEDIT_To.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DateEDIT_To.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.DateEDIT_To.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.DateEDIT_To.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.DateEDIT_To.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.DateEDIT_To.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.DateEDIT_To.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEDIT_To.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.DateEDIT_To.Properties.EditFormat .FormatString = "yyyy-MM-dd";

            this.DateEDIT_To.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.DateEDIT_To.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEDIT_To.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEDIT_To.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.DateEDIT_To.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.DateEDIT_To.Size = new System.Drawing.Size(171, 20);
            this.DateEDIT_To.TabIndex = 1003;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.labelControl1.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.labelControl1.Appearance.BorderColor = System.Drawing.Color.Gray;
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl1.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl1.Appearance.Options.UseBackColor = true;
            this.labelControl1.Appearance.Options.UseBorderColor = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Appearance.Options.UseTextOptions = true;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl1.Location = new System.Drawing.Point(193, 76);
            this.labelControl1.LookAndFeel.SkinName = "Seven Classic";
            this.labelControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(171, 31);
            this.labelControl1.TabIndex = 1004;
            this.labelControl1.Text = "من التاريخ ";
            // 
            // btn_Save
            // 
            this.btn_Save.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btn_Save.Appearance.Font = new System.Drawing.Font("Traditional Arabic", 16F, System.Drawing.FontStyle.Bold);
            this.btn_Save.Appearance.ForeColor = System.Drawing.Color.Green;
            this.btn_Save.Appearance.Options.UseBackColor = true;
            this.btn_Save.Appearance.Options.UseFont = true;
            this.btn_Save.Appearance.Options.UseForeColor = true;
            this.btn_Save.Location = new System.Drawing.Point(13, 138);
            this.btn_Save.LookAndFeel.SkinName = "Foggy";
            this.btn_Save.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btn_Save.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(531, 43);
            this.btn_Save.TabIndex = 1005;
            this.btn_Save.Text = "عرض";
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(2);
            this.gridControl1.Location = new System.Drawing.Point(13, 217);
            this.gridControl1.LookAndFeel.SkinName = "iMaginary";
            this.gridControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(531, 312);
            this.gridControl1.TabIndex = 1006;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn1.Caption = "الصنف";
            this.gridColumn1.FieldName = "Product";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // spinEdit1
            // 
            this.spinEdit1.EditValue = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.spinEdit1.Location = new System.Drawing.Point(373, 108);
            this.spinEdit1.Name = "spinEdit1";
            this.spinEdit1.Properties.Appearance.Options.UseTextOptions = true;
            this.spinEdit1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.spinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEdit1.Size = new System.Drawing.Size(171, 20);
            this.spinEdit1.TabIndex = 1007;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.labelControl2.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.labelControl2.Appearance.BorderColor = System.Drawing.Color.Gray;
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl2.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl2.Appearance.Options.UseBackColor = true;
            this.labelControl2.Appearance.Options.UseBorderColor = true;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.Appearance.Options.UseTextOptions = true;
            this.labelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl2.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl2.Location = new System.Drawing.Point(373, 76);
            this.labelControl2.LookAndFeel.SkinName = "Seven Classic";
            this.labelControl2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(171, 31);
            this.labelControl2.TabIndex = 1004;
            this.labelControl2.Text = "نسبه الاكراميه للطن";
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(283, 189);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.ReadOnly = true;
            this.textEdit1.Size = new System.Drawing.Size(261, 20);
            this.textEdit1.TabIndex = 1008;
            // 
            // textEdit2
            // 
            this.textEdit2.Location = new System.Drawing.Point(12, 189);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Properties.ReadOnly = true;
            this.textEdit2.Size = new System.Drawing.Size(261, 20);
            this.textEdit2.TabIndex = 1008;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(13, 538);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(532, 23);
            this.simpleButton1.TabIndex = 1009;
            this.simpleButton1.Text = "طباعة";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // dateEdit1
            // 
            this.dateEdit1.EditValue = null;
            this.dateEdit1.Location = new System.Drawing.Point(540, 202);
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Size = new System.Drawing.Size(100, 20);
            this.dateEdit1.TabIndex = 1010;
            // 
            // frm_CustomerReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(557, 569);
            this.Controls.Add(this.dateEdit1);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.textEdit2);
            this.Controls.Add(this.textEdit1);
            this.Controls.Add(this.spinEdit1);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.btn_Save);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.DateEDIT_To);
            this.Controls.Add(this.labelControl_Date);
            this.Controls.Add(this.DateEdit_From);
            this.Controls.Add(this.labelControl_Clint);
            this.Controls.Add(this.SearchLookUpEdit_Clint);
            this.Name = "frm_CustomerReport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frm_CustomerReport";
            this.Load += new System.EventHandler(this.frm_CustomerReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.SearchLookUpEdit_Clint.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_From.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_From.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEDIT_To.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEDIT_To.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl_Clint;
        private DevExpress.XtraEditors.SearchLookUpEdit SearchLookUpEdit_Clint;
        private DevExpress.XtraEditors.LabelControl labelControl_Date;
        private DevExpress.XtraEditors.DateEdit DateEdit_From;
        private DevExpress.XtraEditors.DateEdit DateEDIT_To;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btn_Save;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraEditors.SpinEdit spinEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.DateEdit dateEdit1;
    }
}