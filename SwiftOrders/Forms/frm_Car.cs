﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SwiftOrders.DAL;
using DevExpress.XtraEditors;

namespace SwiftOrders.Forms
{
    public partial class frm_Car : Form
    {
        public frm_Car()
        {
            InitializeComponent();
        }
        Car car = new Car();

    

        private void btn_Save_Click(object sender, EventArgs e)
        {
            Save();

        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            New();

        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
         
            car.Delete();
            RefreshItems();

         

        }
        public  void New()
        {
           
            car = new DAL.Car();
            GetData();
     
        }

        public void GetData()
        {
          
            TextEdit_PlateNB.Text = (string)car.PlateNB;
   
            SpinEdit_CargoCap.EditValue = (Double)car.CargoCap;
            SpinEdit_TankCap.EditValue = (Double)car.TankCap;
            TextEdit_CarColor.Text = (string)car.CarColor;
            TextEdit_EngSerial.Text = (string)car.EngSerial;
            TextEdit_ChassisNumber.Text = (string)car.ChassisNumber;
            DateEdit_ModelYear.DateTime = (DateTime)car.ModelYear;
            TextEdit_ModelNumber.Text = (string)car.ModelNumber;
            DateEdit_CurrentRegstrationStartDate.DateTime = (DateTime)car.CurrentRegstrationStartDate;
            DateEdit_CurrentRegstrationEndDate.DateTime = (DateTime)car.CurrentRegstrationEndDate;
            TextEdit_TireSize.Text = (string)car.TireSize;
            LookUpEdit_Driver.EditValue= car.Driver;

        }

        public void SetData()
        {
           
            car.PlateNB = TextEdit_PlateNB.Text;
            car.CargoCap =Convert.ToDouble( SpinEdit_CargoCap.EditValue);
            car.TankCap = Convert.ToDouble(SpinEdit_TankCap.EditValue);
            car.CarColor = TextEdit_CarColor.Text;
            car.EngSerial = TextEdit_EngSerial.Text;
            car.ChassisNumber = TextEdit_ChassisNumber.Text;
            car.ModelYear = DateEdit_ModelYear.DateTime;
            car.ModelNumber = TextEdit_ModelNumber.Text;
            car.CurrentRegstrationStartDate = DateEdit_CurrentRegstrationStartDate.DateTime;
            car.CurrentRegstrationEndDate = DateEdit_CurrentRegstrationEndDate.DateTime;
            car.TireSize = TextEdit_TireSize.Text;
            car.Driver = (int)LookUpEdit_Driver.EditValue;
        }

        public  void Save()
        {
            if (TextEdit_PlateNB .Text.Trim() == string.Empty)
            {
                Master.ShowInformMessseg(this, "خطأ", "يجب ادخال رقم اللوحه");// XtraMessageBox.Show("يجب ادخال رقم اللوحه");
                return;
            }
            if (car.IsNew == true && Master.CheckIfSQLObjectExist(TextEdit_PlateNB.Text, "Car", "PlateNB") == true)
            {
                Master.ShowInformMessseg(this, "خطأ", "هذه السياره مسجله بالفعل ");//  XtraMessageBox.Show("هذه السياره مسجله بالفعل ");
                return;
            }
            SetData();
            car.Save();
            Master.ShowInformMessseg(this, "", "تم الحفظ بنجاح ");// XtraMessageBox.Show("تم الحفظ بنجاح ");
            RefreshItems();
            //Master.LogUserAction(this, car.IsNew.ToString(), car.Name, car.ID.ToString());
        
        }

        private void RefreshItems()
        {
            frm_Drivers.ToLookUp(LookUpEdit_Driver);
            gridControl1.DataSource = Car.ToData();
        }

        private void frm_Car_Load_1(object sender, EventArgs e)
        {
            Master.RestoreGridLayout (gridControl1,this);
            New();
            RefreshItems();
            gridView1.Columns["ID"].Visible = false;
            gridView1.Columns["Driver"].Visible = false;
            gridView1.Columns["PlateNB"].Caption = "رقم اللوحه";
            gridView1.Columns["CargoCap"].Caption = "الحموله";
            gridView1.Columns["TankCap"].Caption = "سعه الوقود ";
            gridView1.Columns["CarColor"].Caption = "اللون ";
            gridView1.Columns["EngSerial"].Caption = "رقم الماتور ";
            gridView1.Columns["ModelYear"].Caption = "سنه الموديل ";
            gridView1.Columns["ModelNumber"].Caption = "رقم الموديل ";
            gridView1.Columns["ModelYear"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            gridView1.Columns["ModelYear"].DisplayFormat.FormatString = "yyyy";
            gridView1.Columns["ChassisNumber"].Caption = "رقم الشاسيه";
            gridView1.Columns["CurrentRegstrationStartDate"].Caption = "بدايه الترخيص";
            gridView1.Columns["CurrentRegstrationEndDate"].Caption = "نهايه الترخيص";
            gridView1.Columns["TireSize"].Caption = "حجم الكاوتش ";

        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {

            if (gridView1.FocusedRowHandle >= 0)
            {
                car = new Car((int)gridView1.GetFocusedRowCellValue("ID"));
                GetData();
            }
        }

        private void frm_Car_FormClosing(object sender, FormClosingEventArgs e)
        {
            Master.SaveGridLayout (gridControl1, this);

        }
    }
}
