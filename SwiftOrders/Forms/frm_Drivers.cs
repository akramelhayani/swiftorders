﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SwiftOrders.Forms
{
    public partial class frm_Drivers : Form
    {
        public frm_Drivers()
        {
            InitializeComponent();
        }

        private void frm_Drivers_Load(object sender, EventArgs e)
        {
            refreshdata();
            gridView1.Columns[0].Visible = false;
            gridView1.Columns[1].Caption  = "الاسم";


        }
        void refreshdata()
        {
            gridControl1.DataSource = Master.SqlDataTable("Select * From Drivers");
        }
        public static void ToLookUp(DevExpress.XtraEditors.LookUpEdit lookUp  )
        {
            lookUp .Properties.DataSource = Master.SqlDataTable("Select * From Drivers");
            lookUp.Properties.ValueMember = "ID";
            lookUp.Properties.DisplayMember = "Name";
            lookUp.Properties.PopulateColumns();
            lookUp.Properties.Columns[1].Caption = "الاسم";
            lookUp.Properties.Columns[0].Visible  = false ;
        }

        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(((DataRowView)e.Row)[1].ToString())) return;

            if (e.RowHandle < 0)
            {
                Master.SQlCmd("Insert into Drivers (Name) values ('" + ((DataRowView)e.Row)[1] + "')");
            }
            if (e.RowHandle >= 0)
            {
                Master.SQlCmd("Update   Drivers set Name  = '" + ((DataRowView)e.Row)[1] + "' where ID = '" + ((DataRowView)e.Row)[0] + "' ");
            }
            refreshdata();
        }
    }
}
